from urllib.parse import urlparse
from sklearn import svm
import numpy as np
from sklearn import preprocessing
from sklearn.feature_extraction.text import HashingVectorizer, CountVectorizer, TfidfVectorizer
from nltk.tokenize import word_tokenize
import re
import sys
import pickle
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import Binarizer
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score
import random

np.random.seed(2000)

def shuffle_in_unison_scary(a, b):
    rng_state = np.random.get_state()
    np.random.shuffle(a)
    np.random.set_state(rng_state)
    np.random.shuffle(b)
    return (a, b)

def process_training_data():

    entities = ["sharisushibar", "restaurante-sol-e-mar", "restaurantedoclubenavaldelisboa", "jardimdosentidos", "aquariumcoimbra", \
                "gaviaonovo", "8healthlounge", "restaurantedonpedro1", "sakeportugal", "clinicadentariajardimdosarcos", \
                "hennessys-pub", "doiscorvos", "salpoente", "eduardomerino", "centromedicodetondela", "vetdinha", "dnuno", \
                "restauranteacantarinha", "cufra", "palaciochiado", "restaurantelilys", "restaurantemarradas", "voltaemeia", \
                "INATEL", "farmaciacodeco", "povolisboa", "espacodoluto", "caldeirasevulcoes", "ladobcafe", "ovitral" \
                ]
    true_dic = []
    arrayX_p = np.array([])
    arrayY_p = np.array([])
    arrayX_n = np.array([])
    arrayY_n = np.array([])

    for fileEn in entities:
        filename = "../manual_annotations/rules_" + fileEn + ".txt"
        entity_file = open(filename, "r", encoding="utf-8")

        for line in entity_file:

            if line == "" or line == "\n":
                continue
   
            if re.search("file", line):
                continue

            if re.search("HOURS2: (.*) ([0-9]+)", line):
                line = re.sub("Horas: ", "", line)

            elif re.search("PHONE3: (.*) ([0-9]+)", line):
                line = re.sub("Telefone: ", "", line)

            m = re.search("ADDRESS1: (.*) ([0-9]+)", line)

            if m:
                if m.group(1) not in true_dic:
                    true_dic.append(m.group(1))
                    if m.group(2) == "1":          
                        arrayX_p = np.append(arrayX_p, [m.group(1)], axis=0) 
                        arrayY_p = np.append(arrayY_p, [1], axis=0) 
                    else:
                        arrayX_n = np.append(arrayX_n, [m.group(1)], axis=0) 
                        arrayY_n = np.append(arrayY_n, [0], axis=0) 
            
            else:

                m2 = re.search("(.*) ([0-9]+)", line)
                if m2.group(1) not in true_dic:
                    true_dic.append(m2.group(1))
               
                    if m2.group(2) == "1":          
                        arrayX_p = np.append(arrayX_p, [m2.group(1)], axis=0) 
                        arrayY_p = np.append(arrayY_p, [1], axis=0) 
                    else:
                        arrayX_n = np.append(arrayX_n, [m2.group(1)], axis=0) 
                        arrayY_n = np.append(arrayY_n, [0], axis=0) 
                

        entity_file.close()

    print(str(len(arrayX_p)))
    print(str(len(arrayX_n)))

    balanced_sample_X = random.choices(arrayX_n, k=len(arrayX_p))
    balanced_sample_Y = random.choices(arrayY_n, k=len(arrayX_p))
    print(len(balanced_sample_X))
    print(len(balanced_sample_Y))

    arrayX = np.concatenate((arrayX_p, balanced_sample_X))
    arrayY = np.concatenate((arrayY_p, balanced_sample_Y))
    print(len(arrayX))
    print(len(arrayY))

    arrayX, arrayY = shuffle_in_unison_scary(arrayX, arrayY)
    print(len(arrayX))
    print(len(arrayY))

    #for el in range(0, len(arrayX)):
    #    print(str(arrayX[el]) + " -> " + str(arrayY[el]))

    with open("arrayX", "wb") as X:
        pickle.dump(arrayX, X, protocol=2)
    with open("arrayY", "wb") as Y:
        pickle.dump(arrayY, Y, protocol=2)
    
def create_train_test(f):

    with open("arrayX", "rb") as X:
        arrayX = pickle.load(X)
    with open("arrayY", "rb") as Y:
        arrayY = pickle.load(Y)

    training_X, testing_X, training_Y, testing_Y = train_test_split(arrayX, arrayY, test_size = 0.2, random_state = 2000)
    f.write(str(training_X.shape) + "\n")
    f.write(str(training_Y.shape) + "\n")
    f.write(str(testing_X.shape) + "\n")
    f.write(str(testing_Y.shape) + "\n")
    
    with open("trainingX", "wb") as train:
        pickle.dump(training_X, train, protocol=2)
    with open("testingX", "wb") as testing:
        pickle.dump(testing_X, testing, protocol=2)
    with open("trainingY", "wb") as train:
        pickle.dump(training_Y, train, protocol=2)
    with open("testingY", "wb") as testing:
        pickle.dump(testing_Y, testing, protocol=2)



def train_svm(vec_type, f):       
    
    with open("trainingX", "rb") as trainX:
        training_X = pickle.load(trainX) 
    with open("trainingY", "rb") as trainY:
        training_Y = pickle.load(trainY)
 
    
    if vec_type == "count":
        f.write("\nSupport Vector Machine Count\n")
        
        f.write("Lets Vectorize!\n")
        vectorizer = CountVectorizer()
        countedX = vectorizer.fit_transform(training_X)

        with open("vectorizerSVC_Count", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets classify!\n")
        parameters = {'kernel':('linear', 'rbf', 'sigmoid'), 'gamma':('scale', 'auto'), 'C':[1, 10, 100]}
        svc = svm.SVC()
        clf = GridSearchCV(svc, parameters)
        clf.fit(countedX, training_Y) 
        f.write(str(clf.best_params_) + "\n")
        f.write("Classified!\n")
        
        filename = "clfSVC_Count"  
    
    
    elif vec_type == "hash":
        f.write("\nSupport Vector Machine Hash\n")

        f.write("Lets Vectorize!\n")
        vectorizer = HashingVectorizer()
        countedX = vectorizer.fit_transform(training_X)

        with open("vectorizerSVC_Hash", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        
        f.write("Lets classify!\n")
        parameters = {'kernel':('linear', 'rbf', 'sigmoid'), 'gamma':('scale', 'auto'), 'C':[1, 10, 100]}
        svc = svm.SVC()
        clf = GridSearchCV(svc, parameters)
        clf.fit(countedX, training_Y)
        f.write(str(clf.best_params_) + "\n")
        f.write("Classified!\n")
        
        filename = "clfSVC_Hash"

    elif vec_type == "tfidf":
        f.write("\nSupport Vector Machine Tfidf\n")

        f.write("Lets Vectorize!\n")
        vectorizer = TfidfVectorizer()
        countedX = vectorizer.fit_transform(training_X)

        with open("vectorizerSVC_Tfidf", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        
        f.write("Lets classify!\n")
        parameters = {'kernel':('linear', 'rbf', 'sigmoid'), 'gamma':('scale', 'auto'), 'C':[1, 10, 100]}
        svc = svm.SVC()
        clf = GridSearchCV(svc, parameters)
        clf.fit(countedX, training_Y)
        f.write(str(clf.best_params_) + "\n")
        f.write("Classified!\n")
        
        filename = "clfSVC_Tfidf" 


    score = cross_val_score(clf, countedX, training_Y, cv=6)
    f.write("Score with the entire dataset = " + str(np.mean(score)) + " +/- " + str(np.std(score)) + "\n")
    
    f.write("Type of Classifier: " + str(clf.best_params_) + "\n")
    
    with open(filename, "wb") as clFile:
        pickle.dump(clf, clFile, protocol=2)

def train_logistic(vec_type, f):

    with open("trainingX", "rb") as trainX:
        training_X = pickle.load(trainX) 
    with open("trainingY", "rb") as trainY:
        training_Y = pickle.load(trainY)
    
    if vec_type == "count":
        f.write("\nLogistic Regression\n")

        f.write("Lets count!\n")
        vectorizer = CountVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerLogis_Count", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets regress logistically :p\n")
        parameters = {'random_state':[200], 'max_iter':[1000], 'C':[1, 10, 100]}
        lr = LogisticRegression()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfLogistic_Count", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)
        
    elif vec_type == "hash":
        f.write("\nLogistic Regression\n")

        f.write("Lets count!\n")
        vectorizer = HashingVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerLogis_Hash", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets regress logistically :p\n")
        parameters = {'random_state':[200], 'max_iter':[1000], 'C':[1, 10, 100]}
        lr = LogisticRegression()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfLogistic_Hash", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    elif vec_type == "tfidf":
        f.write("\nLogistic Regression\n")
        f.write("Lets count!\n")
        vectorizer = TfidfVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerLogis_Tfidf", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets regress logistically :p\n")
        parameters = {'random_state':[200], 'max_iter':[1000], 'C':[1, 10, 100]}
        lr = LogisticRegression()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfLogistic_Tfidf", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    score = cross_val_score(clf, countedX, training_Y, cv=6)
    f.write("Score with the entire dataset = " + str(np.mean(score)) + " +/- " + str(np.std(score)) + "\n")

    f.write("Type of Classifier: " + str(clf.best_params_) + "\n")

def testLogistic(vector_filename, clf_filename, results):

       
    with open("testingX", "rb") as testX:
        testing_X = pickle.load(testX)
    with open("testingY", "rb") as testY:
        testing_Y = pickle.load(testY)
        
    #results.write(str(testing_X.shape) + "\n")
    #results.write(str(testing_Y.shape) + "\n")
        
    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)


    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

    test = np.array([])
            
    for line in testing_X:
        test = np.append(test, [line], axis=0)
            
    x_test = vectorizer.transform(test)
    y_test = clf.predict(x_test)
    #results.write(str(y_test) + "\n")

    score = clf.score(x_test, testing_Y)
    
    results.write("Logistic Regression with " + str(vector_filename) + "\n")
    results.write(str(score) + "\n")            # accuracy

    #results.write(str(["TN", "FP"]) + "\n")
    #results.write(str(["FN", "TP"]) + "\n")
    conf = confusion_matrix(testing_Y, y_test)
    tn, fp, fn, tp = conf.ravel()
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f_measure = 2 * (precision * recall)/(precision + recall)
    results.write(str(precision) + "\n")        #precision
    results.write(str(recall) + "\n")              #recall
    results.write(str(f_measure) + "\n")        #fmeasure

    #results.write("Logistic Regression confusion matrix:\n")
    #results.write(str(conf) + "\n\n\n")



def testSVC(vector_filename, clf_filename, results):
    
    with open("testingX", "rb") as testX:
        testing_X = pickle.load(testX)
    with open("testingY", "rb") as testY:
        testing_Y = pickle.load(testY)
        
    #results.write(str(testing_X.shape) + "\n")
    #results.write(str(testing_Y.shape) + "\n")
        
    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)

    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

    test = np.array([])
            
    for line in testing_X:
        test = np.append(test, [line], axis=0)
            
    x_test = vectorizer.transform(test)
    y_test = clf.predict(x_test)
    #results.write(str(y_test) + "\n")

    score = clf.score(x_test, testing_Y)
    
    results.write("Support Vector Machine with " + str(vector_filename) + "\n")
    results.write(str(score) + "\n")

    #results.write(str(["TN", "FP"]) + "\n")
    #results.write(str(["FN", "TP"]) + "\n")
    conf = confusion_matrix(testing_Y, y_test)
    tn, fp, fn, tp = conf.ravel()
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f_measure = 2 * (precision * recall)/(precision + recall)
    results.write(str(precision) + "\n")        #precision
    results.write(str(recall) + "\n")           #recall
    results.write(str(f_measure) + "\n")        #fmeasure

    #results.write("Logistic Regression confusion matrix:\n")
    #results.write(str(conf) + "\n\n\n")



def train_naive(vec_type, f):

    with open("trainingX", "rb") as trainX:
        training_X = pickle.load(trainX) 
    with open("trainingY", "rb") as trainY:
        training_Y = pickle.load(trainY)
    
    if vec_type == "count":
        f.write("\nNaive Bayes\n")

        f.write("Lets count!\n")
        vectorizer = CountVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerNaive_Count", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        encoder = Binarizer()
        encodedX = encoder.fit_transform(countedX)
        
        with open("encoderNaive_Count", "wb") as vec:
            pickle.dump(encoder, vec, protocol=2)

        f.write("Lets do some Naive Bayes :p\n")
        nb = MultinomialNB()
        clf = nb.fit(encodedX, training_Y)
        f.write("\n\n\n\n\n")

        with open("clfNaive_Count", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)
        
    elif vec_type == "hash":
        f.write("\nNaive Bayes\n")

        f.write("Lets count!\n")
        vectorizer = HashingVectorizer(alternate_sign=False)
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerNaive_Hash", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
                  
        encoder = Binarizer()
        encodedX = encoder.fit_transform(countedX)
        
        with open("encoderNaive_Hash", "wb") as vec:
            pickle.dump(encoder, vec, protocol=2)
      
        
        f.write("Lets do some Naive Bayes :p\n")
        nb = MultinomialNB()
        clf = nb.fit(encodedX, training_Y)

        with open("clfNaive_Hash", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    elif vec_type == "tfidf":
        f.write("\nNaive Bayes\n")
        f.write("Lets count!\n")
        vectorizer = TfidfVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerNaive_Tfidf", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        encoder = Binarizer()
        encodedX = encoder.fit_transform(countedX.toarray())
        
        with open("encoderNaive_Tfidf", "wb") as vec:
            pickle.dump(encoder, vec, protocol=2)
        
        f.write("Lets do some Naive Bayes :p\n")
        nb = MultinomialNB()
        clf = nb.fit(encodedX, training_Y)

        with open("clfNaive_Tfidf", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    score = cross_val_score(clf, countedX, training_Y, cv=6)
    f.write("Score with the entire dataset = " + str(np.mean(score)) + " +/- " + str(np.std(score)) + "\n")

    f.write("Type of Classifier: " + str(clf.get_params()) + "\n")

def testNaive(vector_filename, encoder_filename, clf_filename, results):

       
    with open("testingX", "rb") as testX:
        testing_X = pickle.load(testX)
    with open("testingY", "rb") as testY:
        testing_Y = pickle.load(testY)
        
    #results.write(str(testing_X.shape) + "\n")
    #results.write(str(testing_Y.shape) + "\n")
        
    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)
    
    with open(encoder_filename, "rb") as vec:
        encoder = pickle.load(vec)

    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

    test = np.array([])
            
    for line in testing_X:
        test = np.append(test, [line], axis=0)
    
    #results.write(str(test) + "\n")
    x_test_aux = vectorizer.transform(test)
    x_test = encoder.transform(x_test_aux)
    y_test = clf.predict(x_test)
    #results.write(str(y_test) + "\n")

    score = clf.score(x_test, testing_Y)

    results.write("Naive Bayes with " + str(vector_filename) + "\n")
    results.write(str(score) + "\n")

    #results.write(str(["TN", "FP"]) + "\n")
    #results.write(str(["FN", "TP"]) + "\n")
    conf = confusion_matrix(testing_Y, y_test)
    tn, fp, fn, tp = conf.ravel()
    #print("tp + fp " + str(tp + fp))
    #print("fp " + str(tp))
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f_measure = 2 * (precision * recall)/(precision + recall)
    results.write(str(precision) + "\n")        #precision
    results.write(str(recall) + "\n")           #recall
    results.write(str(f_measure) + "\n")        #fmeasure

    #results.write("Naive Bayes confusion matrix:\n")
    #results.write(str(conf) + "\n\n\n")



def train_decisiontree(vec_type, f):

    with open("trainingX", "rb") as trainX:
        training_X = pickle.load(trainX) 
    with open("trainingY", "rb") as trainY:
        training_Y = pickle.load(trainY)
    
    if vec_type == "count":
        f.write("\nDecision Tree\n")

        f.write("Lets count!\n")
        vectorizer = CountVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerDecisionTree_Count", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets make decisions like a tree :p\n")
        parameters = {'random_state':[200], 'criterion':["gini", "entropy"], 'splitter':["best", "random"], \
                        'max_features':["auto", "sqrt", "log2", None]}
        lr = DecisionTreeClassifier()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfDecisionTree_Count", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)
        
    elif vec_type == "hash":
        f.write("\nDecision Tree\n")

        f.write("Lets count!\n")
        vectorizer = HashingVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerDecisionTree_Hash", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets make decisions like a tree :p\n")
        parameters = {'random_state':[200], 'criterion':["gini", "entropy"], 'splitter':["best", "random"], \
                        'max_features':["auto", "sqrt", "log2", None]}
        lr = DecisionTreeClassifier()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfDecisionTree_Hash", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    elif vec_type == "tfidf":
        f.write("\nDecision Tree\n")
        f.write("Lets count!\n")
        vectorizer = TfidfVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerDecisionTree_Tfidf", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets make decisions like a tree :p\n")
        parameters = {'random_state':[200], 'criterion':["gini", "entropy"], 'splitter':["best", "random"], \
                        'max_features':["auto", "sqrt", "log2", None]}
        lr = DecisionTreeClassifier()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfDecisionTree_Tfidf", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    score = cross_val_score(clf, countedX, training_Y, cv=6)
    f.write("Score with the entire dataset = " + str(np.mean(score)) + " +/- " + str(np.std(score)) + "\n")

    lr = clf.best_estimator_
    f.write(str(lr.decision_path(countedX)))
    f.write("Type of Classifier: " + str(clf.best_params_) + "\n")

def testDecisiontree(vector_filename, clf_filename, results):

       
    with open("testingX", "rb") as testX:
        testing_X = pickle.load(testX)
    with open("testingY", "rb") as testY:
        testing_Y = pickle.load(testY)
        
    #results.write(str(testing_X.shape) + "\n")
    #results.write(str(testing_Y.shape) + "\n")
        
    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)


    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

    test = np.array([])
            
    for line in testing_X:
        test = np.append(test, [line], axis=0)
            
    x_test = vectorizer.transform(test)
    y_test = clf.predict(x_test)
    #results.write(str(y_test) + "\n")

    score = clf.score(x_test, testing_Y)

    results.write("Decision Tree with " + str(vector_filename) + "\n")
    results.write(str(score) + "\n")            # accuracy

    #results.write(str(["TN", "FP"]) + "\n")
    #results.write(str(["FN", "TP"]) + "\n")
    conf = confusion_matrix(testing_Y, y_test)
    tn, fp, fn, tp = conf.ravel()
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f_measure = 2 * (precision * recall)/(precision + recall)
    results.write(str(precision) + "\n")        # precision
    results.write(str(recall) + "\n")           # recall
    results.write(str(f_measure) + "\n")        # fmeasure

    
    #results.write("Decision Tree confusion matrix:\n")
    #results.write(str(conf) + "\n\n\n")

def train_randomforest(vec_type, f):

    with open("trainingX", "rb") as trainX:
        training_X = pickle.load(trainX) 
    with open("trainingY", "rb") as trainY:
        training_Y = pickle.load(trainY)
    
    if vec_type == "count":
        f.write("\nRandom Forest\n")

        f.write("Lets count!\n")
        vectorizer = CountVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerRandomForest_Count", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets make decisions like a random tree :p\n")
        parameters = {'random_state':[200], 'criterion':["gini", "entropy"], 'n_estimators':[10, 100]}
        lr = RandomForestClassifier()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfRandomForest_Count", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)
        
    elif vec_type == "hash":
        f.write("\nRandom Forest\n")

        f.write("Lets hash!\n")
        vectorizer = HashingVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerRandomForest_Hash", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets make decisions like a random tree :p\n")
        parameters = {'random_state':[200], 'criterion':["gini", "entropy"], 'n_estimators':[10, 100]}
        lr = RandomForestClassifier()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfRandomForest_Hash", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    elif vec_type == "tfidf":
        f.write("\nRandom Forest\n")
        f.write("Lets tfidf!\n")
        vectorizer = TfidfVectorizer()
        countedX = vectorizer.fit_transform(training_X)
        
        with open("vectorizerRandomForest_Tfidf", "wb") as vec:
            pickle.dump(vectorizer, vec, protocol=2)
        
        f.write("Lets make decisions like a tree :p\n")
        parameters = {'random_state':[200], 'criterion':["gini", "entropy"], 'n_estimators':[10, 100]}
        lr = RandomForestClassifier()
        clf = GridSearchCV(lr, parameters)
        clf = clf.fit(countedX, training_Y)
        f.write(str(clf.get_params()) + "\n")

        with open("clfRandomForest_Tfidf", "wb") as clfLogi:
            pickle.dump(clf, clfLogi, protocol=2)

    score = cross_val_score(clf, countedX, training_Y, cv=6)
    f.write("Score with the entire dataset = " + str(np.mean(score)) + " +/- " + str(np.std(score)) + "\n")

    f.write("Type of Classifier: " + str(clf.best_params_) + "\n")

def testRandomforest(vector_filename, clf_filename, results):

       
    with open("testingX", "rb") as testX:
        testing_X = pickle.load(testX)
    with open("testingY", "rb") as testY:
        testing_Y = pickle.load(testY)
        
    #results.write(str(testing_X.shape) + "\n")
    #results.write(str(testing_Y.shape) + "\n")
        
    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)


    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

    test = np.array([])
            
    for line in testing_X:
        test = np.append(test, [line], axis=0)
            
    x_test = vectorizer.transform(test)
    y_test = clf.predict(x_test)
    #results.write(str(y_test) + "\n")

    score = clf.score(x_test, testing_Y)

    results.write("Random Forest with " + str(vector_filename) + "\n")
    results.write(str(score) + "\n")      # accuracy

    #results.write(str(["TN", "FP"]) + "\n")
    #results.write(str(["FN", "TP"]) + "\n")
    conf = confusion_matrix(testing_Y, y_test)
    tn, fp, fn, tp = conf.ravel()
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    f_measure = 2 * (precision * recall)/(precision + recall)
    results.write(str(precision) + "\n")    # precision
    results.write(str(recall) + "\n")          # recall
    results.write(str(f_measure) + "\n")    # fmeasure

    #results.write("Random Forest confusion matrix:\n")
    #results.write(str(conf) + "\n\n\n")




def runClassifiers(var, f):

    if var == "help":
            print("get_data: creates arrays of data")
            print("separate_data: creates training and testing dataset")
            print("train_svm: trains both encoders for svms")
            print("train_logistic: trains both encoders for logistics")
            print("test_svm: test all classifiers for svms")
            print("test_logistic: test all classifiers for logistics")
            print("train_all: train all classifiers")
            print("test_all: test all classifiers")

    elif var == "get_data":
            process_training_data()

    elif var == "separate_data":
            create_train_test(f)

    elif var == "train_svm":
            train_svm("count", f)
            train_svm("hash", f)
            train_svm("tfidf", f)

    elif var == "train_logistic":
            train_logistic("count", f)
            train_logistic("hash", f)
            train_logistic("tfidf", f)

    elif var == "test_svm":
            testSVC("vectorizerSVC_Count", "clfSVC_Count", f)
            testSVC("vectorizerSVC_Hash", "clfSVC_Hash", f)
            testSVC("vectorizerSVC_Tfidf", "clfSVC_Tfidf", f)

    elif var == "test_logistic":
            testLogistic("vectorizerLogis_Count", "clfLogistic_Count", f)
            testLogistic("vectorizerLogis_Hash", "clfLogistic_Hash", f)
            testLogistic("vectorizerLogis_Tfidf", "clfLogistic_Tfidf", f)

    elif var == "train_naive":
            train_naive("count", f)
            train_naive("hash", f)
            train_naive("tfidf", f)

    elif var == "test_naive":
            testNaive("vectorizerNaive_Count", "encoderNaive_Count", "clfNaive_Count", f)
            testNaive("vectorizerNaive_Hash", "encoderNaive_Hash", "clfNaive_Hash", f)
            testNaive("vectorizerNaive_Tfidf", "encoderNaive_Tfidf", "clfNaive_Tfidf", f)

    elif var == "train_decisiontree":
            train_decisiontree("count", f)
            train_decisiontree("hash", f)
            train_decisiontree("tfidf", f)

    elif var == "test_decisiontree":
            testDecisiontree("vectorizerDecisionTree_Count", "encoderDecisionTree_Count", "clfDecisionTree_Count", f)
            testDecisiontree("vectorizerDecisionTree_Hash", "encoderDecisionTree_Hash", "clfDecisionTree_Hash", f)
            testDecisiontree("vectorizerDecisionTree_Tfidf", "encoderDecisionTree_Tfidf", "clfDecisionTree_Tfidf", f)

    elif var == "train_randomforest":
            train_randomforest("count", f)
            train_randomforest("hash", f)
            train_randomforest("tfidf", f)

    elif var == "test_randomforest":
            testRandomforest("vectorizerRandomForest_Count", "encoderRandomForest_Count", "clfRandomForest_Count", f)
            testRandomforest("vectorizerRandomForest_Hash", "encoderRandomForest_Hash", "clfRandomForest_Hash", f)
            testRandomforest("vectorizerRandomForest_Tfidf", "encoderRandomForest_Tfidf", "clfRandomForest_Tfidf", f)

    elif var == "train_all":
            train_randomforest("count", f)
            train_randomforest("hash", f)
            train_randomforest("tfidf", f)
            train_decisiontree("count", f)
            train_decisiontree("hash", f)
            train_decisiontree("tfidf", f)
            train_svm("count", f)
            train_svm("hash", f)
            train_svm("tfidf", f)
            train_logistic("count", f)
            train_logistic("hash", f)
            train_logistic("tfidf", f)
            train_naive("count", f)
            train_naive("hash", f)
            train_naive("tfidf", f)
    

    elif var == "test_all":
            testRandomforest("vectorizerRandomForest_Count", "clfRandomForest_Count", f)
            testRandomforest("vectorizerRandomForest_Hash", "clfRandomForest_Hash", f)
            testRandomforest("vectorizerRandomForest_Tfidf", "clfRandomForest_Tfidf", f)
            testDecisiontree("vectorizerDecisionTree_Count", "clfDecisionTree_Count", f)
            testDecisiontree("vectorizerDecisionTree_Hash", "clfDecisionTree_Hash", f)
            testDecisiontree("vectorizerDecisionTree_Tfidf", "clfDecisionTree_Tfidf", f)
            testSVC("vectorizerSVC_Count", "clfSVC_Count", f)
            testSVC("vectorizerSVC_Hash", "clfSVC_Hash", f)    
            testSVC("vectorizerSVC_Tfidf", "clfSVC_Tfidf", f)
            testLogistic("vectorizerLogis_Count", "clfLogistic_Count", f)
            testLogistic("vectorizerLogis_Hash", "clfLogistic_Hash", f)
            testLogistic("vectorizerLogis_Tfidf", "clfLogistic_Tfidf", f)
            testNaive("vectorizerNaive_Count", "encoderNaive_Count", "clfNaive_Count", f)
            testNaive("vectorizerNaive_Hash", "encoderNaive_Hash", "clfNaive_Hash", f)
            testNaive("vectorizerNaive_Tfidf", "encoderNaive_Tfidf", "clfNaive_Tfidf", f)

def testML_noNB(name, vector_filename, clf_filename):

    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)
    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

       
    with open("../UserWebsites", "rb") as f:
        user_list = pickle.load(f)          


    entities = []
    for element in user_list:
        split_url = urlparse(element)
        url = split_url.netloc
        url = re.sub("www.", "", url)
        url = re.sub(".com", "", url)
        url = re.sub(".pt", "", url)
        url = re.sub(".net", "", url)
        entities.append(url)

    name = "../results/User_MLResults_Test_" + name + ".txt"
    nltk_log_file = open(name, "w")

    for fileEn in entities:
        filename = "../user/entities/rules1_" + fileEn + ".txt"
        entity_file = open(filename, "r", encoding="utf-8")
        nltk_log_file.write(fileEn + "\n")
        
        for line in entity_file:
            original = [line]
            
            # If the rules identified a possible hour or phone candidate the system will ignore it 
            # since the language model will only classify the address attribute values found by the
            # regular expressions with the tag -> Morada!
            if not re.search("ADDRESS1: ", line):
                continue

            # If line is empty skip it
            if line == "" or line == "\n":
                continue

            # If line identifies a file then skip it as it is irrelevant for the classifier 
            if re.search(r'\bfile', line):
                continue

            x_test = vectorizer.transform(original)
            y_test = clf.predict(x_test)
            print(str(y_test))

    nltk_log_file.close()


def testML_NB(name, vector_filename, encoder_filename, clf_filename):

       
    with open(vector_filename, "rb") as vec:
        vectorizer = pickle.load(vec)  
    with open(encoder_filename, "rb") as vec:
        encoder = pickle.load(vec)
    with open(clf_filename, "rb") as clFile:
        clf = pickle.load(clFile)

        
    with open("../UserWebsites", "rb") as f:
        user_list = pickle.load(f)          

    entities = []
    for element in user_list:
        split_url = urlparse(element)
        url = split_url.netloc
        url = re.sub("www.", "", url)
        url = re.sub(".com", "", url)
        url = re.sub(".pt", "", url)
        url = re.sub(".net", "", url)
        entities.append(url)

    name = "../results/User_MLResults_Test_" + name + ".txt"
    nltk_log_file = open(name, "w")

    for fileEn in entities:
        filename = "../user/entities/rules1_" + fileEn + ".txt"
        entity_file = open(filename, "r", encoding="utf-8")
        nltk_log_file.write(fileEn + "\n")
        
        for line in entity_file:
            original = [line]
            
            # If the rules identified a possible hour or phone candidate the system will ignore it 
            # since the language model will only classify the address attribute values found by the
            # regular expressions with the tag -> Morada!
            if not re.search("ADDRESS1: ", line):
                continue

            # If line is empty skip it
            if line == "" or line == "\n":
                continue

            # If line identifies a file then skip it as it is irrelevant for the classifier 
            if re.search(r'\bfile', line):
                continue

            x_test_aux = vectorizer.transform(original)
            x_test = encoder.transform(x_test_aux)
            y_test = clf.predict(x_test)
            print(str(y_test))

    nltk_log_file.close()

 

import time

#with open("resultsClassifiers_all.txt", "w") as f:
#    f.write(str(time.asctime(time.localtime(time.time()))) + "\n")

#    runClassifiers(sys.argv[1], f)

#    f.write(str(time.asctime(time.localtime(time.time()))) + "\n")

if sys.argv[1] == "prepare":
    with open("resultsClassifiers_prepare.txt", "w") as f:
        f.write(str(time.asctime(time.localtime(time.time()))) + "\n")

        print("Get train dataset")
        runClassifiers("get_data", f)
        print("Divide data by train and test dataset")
        runClassifiers("separate_data", f)
        
        f.write(str(time.asctime(time.localtime(time.time()))) + "\n")



elif sys.argv[1] == "train":
    with open("resultsClassifiers_train.txt", "w") as f:
        f.write(str(time.asctime(time.localtime(time.time()))) + "\n")

        print("Train Classifiers")
        runClassifiers("train_all", f)
        
        f.write(str(time.asctime(time.localtime(time.time()))) + "\n")


elif sys.argv[1] == "test":
    with open("resultsClassifiers_test.txt", "w") as f:
        f.write(str(time.asctime(time.localtime(time.time()))) + "\n")

        print("Test Classifiers")
        runClassifiers("test_all", f)
        
        f.write(str(time.asctime(time.localtime(time.time()))) + "\n")


elif sys.argv[1] == "user_test":

    testML_noNB("RandomForest", "vectorizerRandomForest_Count", "clfRandomForest_Count")
    testML_noNB("RandomForest", "vectorizerRandomForest_Hash", "clfRandomForest_Hash")
    testML_noNB("RandomForest", "vectorizerRandomForest_Tfidf", "clfRandomForest_Tfidf")
    testML_noNB("DecisionTree", "vectorizerDecisionTree_Count", "clfDecisionTree_Count")
    testML_noNB("DecisionTree", "vectorizerDecisionTree_Hash", "clfDecisionTree_Hash")
    testML_noNB("DecisionTree", "vectorizerDecisionTree_Tfidf", "clfDecisionTree_Tfidf")
    testML_noNB("SVM", "vectorizerSVC_Count", "clfSVC_Count")
    testML_noNB("SVM", "vectorizerSVC_Hash", "clfSVC_Hash")    
    testML_noNB("SVM", "vectorizerSVC_Tfidf", "clfSVC_Tfidf")
    testML_noNB("LogisticRegression", "vectorizerLogis_Count", "clfLogistic_Count")
    testML_noNB("LogisticRegression", "vectorizerLogis_Hash", "clfLogistic_Hash")
    testML_noNB("LogisticRegression", "vectorizerLogis_Tfidf", "clfLogistic_Tfidf")
    testML_NB("NaiveBayes", "vectorizerNaive_Count", "encoderNaive_Count", "clfNaive_Count")
    testML_NB("NaiveBayes", "vectorizerNaive_Hash", "encoderNaive_Hash", "clfNaive_Hash")
    testML_NB("NaiveBayes", "vectorizerNaive_Tfidf", "encoderNaive_Tfidf", "clfNaive_Tfidf")

