#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import BeautifulSoup
import pickle
from bs4 import BeautifulSoup
from urllib.parse import urlparse
# Import regular expressions
import re
# Import to handle folders and files in them
import os
import sys
import chardet
from bs4 import UnicodeDammit

def addressStarter(p):
                return re.search(r"\bRUA\b.*[0-9]+|\bR\. .*[0-9]+|\bR\b.*[0-9]+|\bPRAÇA\b.*[0-9]+|\bP\. .*[0-9]+|\bP\b.*[0-9]+|\bPRAÇETA\b.*[0-9]+|\bLARGO\b.*[0-9]+|\bLUGAR\b.*[0-9]+|\bQUINTA\b.*[0-9]+|\bQ\. .*[0-9]+|\bQ\b.*[0-9]+|\bS[ÍI]TIO\b.*[0-9]+|\bAVENIDA\b.*[0-9]+|\bAV\. .*[0-9]+|\bAV\b.*[0-9]+", str(p).upper())

# Runs RE rules and decides if it is an address
def isAddress(previous_p, p):
                return addressStarter(p) or re.search("[0-9]{4}–[0-9]{3} ", str(p).upper()) or (addressStarter(previous_p) and re.search("[0-9]{4}–[0-9]{3} ", str(p).upper())) 

# Runs RE rules and decides if it is an opening schedule
def isOpeningHours(pre_previous_p, previous_p, p, next_p, f):

                if re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper())and not addressStarter(p) and not addressStarter(previous_p):

                        return re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper()) and not addressStarter(p) and not addressStarter(previous_p)

                elif re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(next_p).upper()) and not addressStarter(p) and not addressStarter(next_p):

                        return re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(next_p).upper()) and not addressStarter(p) and not addressStarter(next_p)


                elif re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(pre_previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper()) and not addressStarter(p) and not addressStarter(pre_previous_p):

                        return re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(pre_previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper()) and not addressStarter(p) and not addressStarter(pre_previous_p)

                return re.search(r'(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b).*(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)', str(p).upper()) and not addressStarter(p)


def isPhone(p):
                return re.search(r"\b[+]{0,1}[(]{0,1}[0-9]{0,3}[)]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}\b", str(p)) \
                        or re.search(r"\b[+]{0,1}[(]{0,1}[0-9]{0,3}[)]{0,1}[0-9]{2}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}\b", str(p)) \
                        or re.search(r"\b[+]{0,1}[(]{0,1}[0-9]{0,3}[)]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{1}\b", str(p))
        

with open("UserWebsites", "rb") as f:
    user_list = pickle.load(f)                  

entities = []
for element in user_list:
    split_url = urlparse(element)
    url = split_url.netloc
    url = re.sub("www.", "", url)
    url = re.sub(".com", "", url)
    url = re.sub(".pt", "", url)
    url = re.sub(".net", "", url)
    entities.append(url)

# HTML files home folder
filesLocation = "user/user/files/"
counter = 0

letter = sys.argv[1]
print(entities)
if letter == "1":

        entities_files = []
        current_entity = None

        for entity in entities:
                name_entity = "user/entities/" + entity + ".txt"
                entities_files.append(open(name_entity, "w", encoding="UTF-8"))

        # Goes file by file in numerical order
        while counter != len(os.listdir(filesLocation)):
                        counter += 1
                        filename = "file" + str(counter) + ".txt"

                        # Opens specific file to read
                        analyze = open(os.path.join(filesLocation, filename), "rb")

                        # Takes out first line that is the URL
                        url = analyze.readline().decode("UTF-8")
                        encoding = analyze.readline().decode("UTF-8")
                        print(encoding)
                        #writingAttr.write(str(url) + "\n")

                        # Check if file is a selected entity in that case we want to save it
                        for entity in entities:
                                m = re.search(entity, url.lower())
                                if m:
                                        current_entity = entity
                        
                        content = analyze.read()

                        soup = BeautifulSoup(content, "html.parser")

                        # Eliminate script and style portions of the HTML as they are irrelevant usually
                        for node in soup.find_all(['script', 'style']):
                                node.decompose()

                        if current_entity != None:
                                entity_file = entities_files[entities.index(current_entity, 0, len(entities))]
                                entity_file.write("\n" + filename + "\n")

                        print(current_entity)
                        # Follows all strings and analyzes it
                        for p in soup.stripped_strings:
                                try:
                                        if current_entity != None:
                                                entity_file.write(p + "\n")
                                except Exception as e:
                                        print(e)

                        current_entity = None

                        # Close current HTML file and go to next
                        analyze.close()

        for entity in entities_files:
                entity.close()


elif letter == "2":

        bad_phrase = 0
        prov_phrase = ""

        for entity in entities:
                print(entity)
                name_entity_rules = "user/entities/" + entity + ".txt"
                entityFileRules = open(name_entity_rules, "r")

                name_entity_rules1 = "user/entities/rules_" + entity + ".txt"
                entityFileRules1 = open(name_entity_rules1, "w")

                for line in entityFileRules:

                        if line == "" or line == "\n":
                                continue

                        if re.search(r'\bfile', line):
                                entityFileRules1.write(str(line))
                                continue

                        line_split = line.split()
                        attribute_type = line_split[-1].isdigit()
                        #print(attribute_type)

                        if attribute_type == True and bad_phrase == 0:
                                entityFileRules1.write(str(line))

                        elif attribute_type == True and bad_phrase == 1:
                                entityFileRules1.write(str(prov_phrase))
                                bad_phrase = 0
                                prov_phrase = ""

                        else:
                                prov_phrase = prov_phrase + line.rstrip('\n') + " "
                                
                                bad_phrase = 1

                entityFileRules.close()
                entityFileRules1.close()

elif letter == "3":

        # Apply rules to annotated files
        for entity in entities:
                name_entity = "user/entities/" + entity + ".txt"
                entityFile = open(name_entity, "r")

                name_entity_rules = "user/entities/rules1_" + entity + ".txt"
                entityFileRules = open(name_entity_rules, "w")

                previous_line = ""
                pre_previous_line = ""
                flags = {0: 0, 1: 0, 2: 0}

                lines = entityFile.readlines()
                for ind in range(0, len(lines)-1):
                        lines[ind] = re.sub("-", "–", lines[ind])
                        line_split = lines[ind].split()
                        p =  " ".join(line_split[0:-1])

                        previous_line_split = previous_line.split()
                        previous_p =  " ".join(previous_line_split[0:-1])

                        pre_previous_line_split = pre_previous_line.split()
                        pre_previous_p =  " ".join(pre_previous_line_split[0:-1])

                        next_line = re.sub("-", "–", lines[ind+1])
                        next_line_split = next_line.split()
                        next_p =  " ".join(next_line_split[0:-1])

                        # Morada com RE
                        if isAddress(previous_p, p):
                                #p = parse_address(p)
                                entityFileRules.write("ADDRESS1: " + str(lines[ind]))
                                flags[0] += 1

                        # Horario com RE
                        if isOpeningHours(pre_previous_p, previous_p, p, next_p, entityFileRules):
                                entityFileRules.write("HOURS2: " + str(lines[ind]))
                                flags[1] += 1

                        # Horario com RE
                        if isPhone(p):
                                entityFileRules.write("PHONE3: " + str(lines[ind]))
                                flags[2] += 1

                        if not isAddress(previous_p, p) and not isOpeningHours(pre_previous_p, previous_p, p, next_p, entityFileRules) and not isPhone(p):
                                entityFileRules.write(str(lines[ind]))
                
                        pre_previous_line = previous_line
                        previous_line = lines[ind]

                #print("Flags: " + str(flags[0]) + " " + str(flags[1]) + " " + str(flags[2]))
                if flags[2] == 0:
                        print(str(entity) + " PHONE: NOT FOUND")

                if flags[1] == 0:
                        print(str(entity) + " OPENING HOURS: NOT FOUND")

                if flags[0] == 0:
                        print(str(entity) + " ADDRESS: NOT FOUND")

                entityFile.close()
                entityFileRules.close()
