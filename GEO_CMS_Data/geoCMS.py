# -*- coding: utf8 -*-
import nltk
#nltk.download()
#nltk.data.path.append("/afs/l2f.inesc-id.pt/home/msbfa/nltk_data");
from dbfread import DBF
import re
import json
from nltk import word_tokenize

moradas = open("addresses_cms.txt", "w")
dup = {}
lines = 0
duplicates = 0

with DBF("GEO_CMS/Dados_Concelho_Almeirim/Dados_Concelho_Almeirim.dbf", encoding="utf-8") as geo1:
	for record in geo1:
		if record['Descricao'] not in dup.keys() and not re.search("^[0-9]+\n$", record['Descricao']):
			dup[record['Descricao']] = 1
			moradas.write(record['Descricao'] + "\n")
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0

with DBF("GEO_CMS/DOC_255158_ANX_98053/nome_ruas.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['nome_rua'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0

with DBF("GEO_CMS/ENG_MARIA_ALMEIDA_IST_CM_MONTIJO_N_POL_MORADAS/NUMEROS_POLICIA.dbf", encoding="utf-8") as geo3:
	for record in geo3:
		if record['TOPONIMO'] not in dup.keys() and not re.search("^[0-9]+\n$", record['TOPONIMO']):
			dup[record['TOPONIMO']] = 1
			moradas.write(record['TOPONIMO'] + "\n")  
			lines += 1
		else:
			duplicates += 1


print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0  

with DBF("GEO_CMS/Inf_Sabugal/rede_viaria.dbf", encoding="utf-8") as geo4:
	for record in geo4:
		if record['desig'] not in dup.keys() and not re.search("^[0-9]+\n$", record['desig']):
			dup[record['desig']] = 1
			moradas.write(record['desig'] + "\n")  
			lines += 1
		else:
			duplicates += 1


print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0  

with DBF("GEO_CMS/Informação_Machico/Vias_Toponimia/vias_mm.dbf", encoding="utf-8") as geo5:
	for record in geo5:
		if record['Arruamento'] not in dup.keys() and not re.search("^[0-9]+\n$", record['Arruamento']):
			dup[record['Arruamento']] = 1
			moradas.write(record['Arruamento'] + "\n") 
			lines += 1
		else:
			duplicates += 1


print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0  

with DBF("GEO_CMS/informação_toponimica_Alcacer-25-05-2020/Toponimia_Alcacer-26-05-2020.dbf", encoding="utf-8") as geo6:
	for record in geo6:
		if record['toponimia'] not in dup.keys() and not re.search("^[0-9]+\n$", record['toponimia']):
			dup[record['toponimia']] = 1
			moradas.write(record['toponimia'] + "\n") 
			lines += 1
		else:
			duplicates += 1


print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0  

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/Toponimia da localidade da Lajeosa do Mondego.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1

		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0  

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/toponimia_Aldeia_da_Serra.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/TOPONIMIA_BARACAL.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['toponimia'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/TOPONIMIA_CADAFAZ.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 


with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/Toponimia_Casas_Soeiro.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ÑOME'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/TOPONIMIA_GALISTEU.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/Toponimia_macal.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/toponimia_minhocal.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/TOPONIMIA_RAPA.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/TOPONIMIA_SOUTOMONINHO.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/Toponimia_Vide_Entre_Vinhas.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['ARTERIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/sapotransfer-5a5b044b29687GB/vias_prados.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['TOPONIMIA'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/Toponimia e npolicia_Guilheiro/ruas_Guilheiro.dbf", encoding="utf-8") as geo7:
	for record in geo7:
		if record['DESIGNACAO'] not in dup.keys() and not re.search("^[0-9]+\n$", record['DESIGNACAO']):
			dup[record['DESIGNACAO']] = 1
			moradas.write(record['DESIGNACAO'] + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 


with DBF("GEO_CMS/toponimia_fnd/toponimia_fnd_etrs89.dbf", encoding="utf-8") as geo8:
	for record in geo8:
		if record['NOME'] not in dup.keys() and not re.search("^[0-9]+\n$", record['NOME']):
			dup[record['NOME']] = 1
			moradas.write(record['NOME'] + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

#moradas.write("\n\n\n\n\n")
with DBF("GEO_CMS/Toponimia_Freches/rua_Freches.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['Designação'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/Toponimia_Garcia Joanes e Feital/Rua_Feital.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['designação'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/Toponimia_Póvoa do Concelho/rua_Póvoa do Concelho.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['Designação'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/Toponimia_Vila Franca das Naves/rua_VFNaves.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['Designação'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/VCONDE/VCONDE_MORADAS.dbf", encoding="utf-8") as geo9:
	for record in geo9:
		if record['LABEL'] not in dup.keys() and not record['TIPO_VIA'] == "URB" and not record['TIPO_VIA'] == "VIELA" and not re.search("^[0-9]+\n$", record['LABEL']):
			dup[record['LABEL']] = 1
			moradas.write(record['LABEL'] + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/N_policia.dbf", encoding="utf-8") as geo10:
	for record in geo10:
		tmp = record['Local_']
		tmp = re.sub(",.*$", "", tmp)
		tmp = re.sub(" - .*$", "", tmp)
		tmp = re.sub(" [0-9]+$", "", tmp)
		if tmp not in dup.keys() and not re.search("^[0-9]+\n$", tmp) and not re.search("urbanização", tmp.lower()) and not len(tmp) < 6:
			dup[tmp] = 1
			moradas.write(tmp + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 			

with open('GEO_CMS/bancos.geojson', 'r', encoding="utf-8") as g:
	data = g.read()
	obj = json.loads(data)
	for feature in obj["features"]:
		tmp = feature["properties"]["Morada"]
		tmp = re.sub(",.*$", "", tmp)
		tmp = re.sub(" - .*$", "", tmp)
		tmp = re.sub(" [0-9]+$", "", tmp)
		if tmp not in dup.keys() and not re.search("^[0-9]+\n$", tmp) and not re.search("urbanização", tmp.lower()) and not len(tmp) < 6:
			dup[tmp] = 1
			moradas.write(tmp + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with open('GEO_CMS/espacoanimal.geojson', 'r', encoding="utf-8") as g:
	data = g.read()
	obj = json.loads(data)
	for feature in obj["features"]:
		tmp = feature["properties"]["Morada"]
		tmp = re.sub(",.*$", "", tmp)
		if tmp not in dup.keys() and not re.search("^[0-9]+\n$", tmp) and not re.search("urbanização", tmp.lower()) and not len(tmp) < 6:
			dup[tmp] = 1
			moradas.write(tmp + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with open('GEO_CMS/pgdmportaldadosabertos2atualizacoesequipamentosculturais.geojson', 'r', encoding="utf-8") as g:
	data = g.read()
	obj = json.loads(data)
	for feature in obj["features"]:
		tmp = feature["properties"]["Morada"]
		tmp = re.sub(",.*$", "", tmp)
		if tmp not in dup.keys() and not re.search("^[0-9]+\n$", tmp) and not re.search("urbanização", tmp.lower()) and not len(tmp) < 6:
			dup[tmp] = 1
			moradas.write(tmp + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with open('GEO_CMS/pgdmportaldadosabertos2atualizacoesinformacaocomunicacao.geojson', 'r', encoding="utf-8") as g:
	data = g.read()
	obj = json.loads(data)
	for feature in obj["features"]:
		tmp = feature["properties"]["Morada"]
		tmp = re.sub(",.*$", "", tmp)
		if tmp not in dup.keys() and not re.search("^[0-9]+\n$", tmp) and not re.search("urbanização", tmp.lower()) and not len(tmp) < 6:
			dup[tmp] = 1
			moradas.write(tmp + "\n") 
			lines += 1
		else:
			duplicates += 1

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/2020_06_numpol_areateste_estudante/alfragide.dbf", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['NOME_COMPL'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/toponimia.dbf", encoding="utf-8") as g:
	for line in g:
		try:
			record = bytes(line['rua'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with open("GEO_CMS/toponimia(1).xlsx", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['Nomes_de_R'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/Toponimia(2).dbf", encoding="utf-8") as g:
	for line in g:
		try:
			record = bytes(line['Rua'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with open("GEO_CMS/Toponimia(3).xlsx", encoding="ISO8859-1") as g:
	for line in g:
		try:
			record = bytes(line['Ruas'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

with DBF("GEO_CMS/Toponimia(4).dbf", encoding="utf-8") as g:
	for line in g:
		try:
			record = bytes(line['Rua'], encoding='utf-8').decode(encoding='utf-8')

			if record not in dup.keys() and not re.search("^[0-9]+\n$", record):
				dup[record] = 1
				moradas.write(record + "\n")
				lines += 1
			else:
				duplicates += 1
		except:
			pass

print("Lines: " + str(lines) + " Duplicates: " + str(duplicates))
lines = 0
duplicates = 0 

moradas.close()

moradas = open("addresses_cms.txt", "r")
num_words = []

for line in moradas:
	if len(word_tokenize(line)) == 0:
		continue
	if len(word_tokenize(line)) == 11:
		print(line)
	num_words.append(len(word_tokenize(line)))


print("Number of streets analyzed: ", len(num_words))
print("Average word length: ", sum(num_words)/len(num_words))
print("Min word length: ", min(num_words))
print("Max word length: ", max(num_words))


