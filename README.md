# Enrichment of Location Databases

Owner: Maria Sofia Barros Feio de Almeida (83516) sofia.feio.almeida@gmail.com / maria.d.almeida@tecnico.ulisboa.pt

## Packages Instalation

pip install dbfread
pip install Scrapy
pip install nltk
pip install -U scikit-learn

## Description

Geographic databases, such as those powered by OpenStreeMap (OSM) (https://www.openstreetmap.org/) contain much useful information on many locations and points of interest around the globe. This information, however, is mostly limited to geographic coordinates and, at best, a category describing the type of location.

The goal of this dissertation is to design and implement an automated solution to complement the information present in OSM with other types of data found on the Web.

For example, take a location such as "Cinema Ideal". You can find its coordinates and a link to the official website, but not, for instance, its opening hours or ticket prices. Your solution should be able to follow the link to the website and extract such information.


## Approach

- Entities in [open street maps][osm] can have many fields (a.k.a. [*tags*][tags]). We are interested in a subset of specific [map features][features], namely:
    - addresses
    - annotation
    - name (only *alt_name*)
    - other non-osm information: facebook page, photos, etc.
- Given a node in a map, not all of the above fields are usually filled: our goal is to fill those missing
- To this effect we can take the information available for an entity and:
    - perform a search on a search engine API (e.g. [Bing](https://azure.microsoft.com/en-us/services/cognitive-services/bing-web-search-api/)
    - from the results, discover the official entity website (**problem 1**)
    - from the web site extract the missing information (**problem 2** ---  we should probably start with this)

### Problem 1

To solve this, we can:
- use a set of heuristics or
- build a classifier, trained with the entities for which we know the official website
    - given a web page, classify it as "official" or "non-official"

### Problem 2

To solve this, we can:
- use heuristics and regular expressions for the more regular types of data (e.g. phone numbers)
- build a classifier for each tag, trained with the data that we do have from the entities
    - given a candidate string/node, classify it as filling that tag or not
- OR build a classifier that, given a string, yields that tag to which it belongs

## General plan

- Read bibliography (see below)
    - Read and structure what you have read
- Get familiar with [OSM][osm]
    - Take a look at the [wiki][wiki]
    - Dowload a piece of data, for developing (e.g. Lisbon)
    - Take a look at the data
    - Extract only the data that matters to this work (possibly convert all to `nodes` and use the [`.osm` format][.osm], which is XML thus easier to read)
        - Using one the [data conversion tools][conv]
        - e.g. `osmconvert64 --all-to-nodes --drop-author PT.pbf > PT.osm`
- Implement a proptotype
    - *problem 1*: 
        - build a dataset of official and non-official web pages
        - train and deploy a classifier
    - *problem 2*:
        - build a dataset with OSM data that is complete and for which we can find the websites
        - delete some attributes
        - build a classifier/heuristics
- Write the report
- Implement the full proposal
- Dowload some countries for testing (Portugal, France, Brasil)
- Experiment
- Write the report

## Bibliography (in order of probable interest)

### For problem 2

I could not find anything exactly like we are doing, which is "slot filling applied directly to OSM". The closest are papers for general slot filling tasks.

- [Knowledge base population: successful approaches and challenges](https://dl.acm.org/citation.cfm?id=2002618)
    - Survey of KBP techniques: we are interested in slot filling techniques
- [Leopard — A baseline approach to attribute prediction and validation for knowledge graph population](https://www.sciencedirect.com/science/article/pii/S1570826818300684)
    - A similar task to our own
- [Comparing convolutional neural networks to traditional models for slot filling](https://www.aclweb.org/anthology/papers/N/N16/N16-1097/)
    - The title says it all. May help us decide if we should go with neural networks or not
- [Knowledge Base Population (KBP)](https://nlp.stanford.edu/projects/kbp/)
    - Webpage from the Stanford NLP group: we are interested in the papers about *slot filling*
- [OpenStreetMap: User-Generated Street Maps](https://ieeexplore.ieee.org/abstract/document/4653466)
    - standard reference for OSM
- [A Comprehensive Framework for Intrinsic OpenStreetMap Quality Analysis](https://onlinelibrary.wiley.com/doi/full/10.1111/tgis.12073)
    - Mostly for reference and motivation: lists the problems of data quality in OSM and how to measure them
    - Another similar paper: [Towards quality metrics for OpenStreetMap](https://dl.acm.org/citation.cfm?id=1869875)
    - And another: [How Good is Volunteered Geographical Information? A Comparative Study of OpenStreetMap and Ordnance Survey Datasets](https://journals.sagepub.com/doi/abs/10.1068/b35097?casa_token=BCCZlPev6lkAAAAA%3AAIZtz3n8xhcftgQk2qyDrf0mYQ7rc3TGIpqGABRUi-V6im5U4ckJH7Evina-b1r5gG011ONE-Jr1&)

### For problem 1

I could not find anything on official website detection, but there is plenty of *phishing* website detection, which *might be* related. For instance:
- [Detection of phishing webpages based on visual similarity](https://dl.acm.org/citation.cfm?id=1062868)
- [Intelligent Phishing Website Detection System using Fuzzy Techniques](https://ieeexplore.ieee.org/abstract/document/4530019)
- [An Intelligent Anti-phishing Strategy Model for Phishing Website Detection](https://ieeexplore.ieee.org/abstract/document/6258133)
- [Survey on web spam detection: principles and algorithms](https://dl.acm.org/citation.cfm?id=2207252)

There are also works on detecting web page quality. Most related to health sites (which is too specific for us):
- [Commonly cited website quality criteria are not effective at identifying inaccurate online information about breast cancer](https://onlinelibrary.wiley.com/doi/full/10.1002/cncr.23308)
    - Contains many references for the quality of health related sites

These two are on other topics:
- [An evaluation of Information quality frameworks for the World Wide Web](https://eprints.soton.ac.uk/262908/)
    - Compares several evaluation frameworks, but is not concerned with the evaluation itself (so, no experiments)
- [Web Quality Index (WQI) for official tourist destination websites. Proposal for an assessment system](https://www.sciencedirect.com/science/article/pii/S2211973613000512)
    - Proposes quality metrics for tourism websites. Nonthing is automatic.


[osm]: https://www.openstreetmap.org/
[wiki]: https://wiki.openstreetmap.org/wiki/Main_Page
[conv]: https://wiki.openstreetmap.org/wiki/Downloading_data
[.osm]: https://wiki.openstreetmap.org/wiki/OSM_XML
[tags]: https://wiki.openstreetmap.org/wiki/Tags
[features]: https://wiki.openstreetmap.org/wiki/Map_Features