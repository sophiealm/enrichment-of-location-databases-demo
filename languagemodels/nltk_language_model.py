# https://www.kaggle.com/alvations/n-gram-language-model-with-nltk
from urllib.parse import urlparse
from nltk.util import pad_sequence
from nltk.util import bigrams
from nltk.util import ngrams
from nltk.util import everygrams
from nltk.lm.preprocessing import pad_both_ends
from nltk.lm.preprocessing import flatten
from nltk.lm.preprocessing import padded_everygram_pipeline
from nltk import word_tokenize, sent_tokenize 
import re
import time
import sys
import pickle
from nltk.lm import KneserNeyInterpolated, WittenBellInterpolated
from nltk.lm import Laplace
import math

start = time.time()
print(sys.argv)

def trainLMs():
    # First dataset POSITIVE
    text = open("../GEO_CMS_Data/addresses_cms.txt", "r", encoding="latin-1")
    corpus = ""
    num_addresses = 0
    num_tokens_addr = []
    num_tokens_dic = {}

    for line in text:
        if line != "\n":
            num = len(word_tokenize(line))
            if num not in num_tokens_dic:
                num_tokens_dic[num] = 1
            else:
                num_tokens_dic[num] += 1
            num_tokens_addr.append(num)

        line = re.sub(r'\b[0-9]+\b', "NUM", line)
        line = line.rstrip("\n")
        corpus += line + ". "


    # Second dataset POSITIVE
    text2 = open("../yellow/paiStreets.txt", "r", encoding="utf-8")
    for line in text2:
        if line != "\n":
            num = len(word_tokenize(line))
            if num not in num_tokens_dic:
                num_tokens_dic[num] = 1
            else:
                num_tokens_dic[num] += 1
            num_tokens_addr.append(num)

        line = re.sub(r'\b[0-9]+\b', "NUM", line)
        line = line.rstrip("\n")
        corpus += line + ". "


    print("Max num of tokens per street: " + str(max(num_tokens_addr)))
    print("Min num of tokens per street: " + str(min(num_tokens_addr)))
    print("Num streets: " + str(len(num_tokens_addr)))
    print("Num tokens in dictionary: " + str(num_tokens_dic))

    tokenized_text = [list(map(str.lower, word_tokenize(sent))) 
                                      for sent in sent_tokenize(corpus)]


    n = 2
    model_k = KneserNeyInterpolated(n)
    model_w = WittenBellInterpolated(n)
    model_n = Laplace(n)

    train_data, padded_sents = padded_everygram_pipeline(n, tokenized_text)

    #for i in list(train_data):
    #       print(list(i))
    #print(list(padded_sents))

    model_k.fit(train_data, padded_sents)
    train_data, padded_sents = padded_everygram_pipeline(n, tokenized_text)
    model_w.fit(train_data, padded_sents)
    train_data, padded_sents = padded_everygram_pipeline(n, tokenized_text)
    model_n.fit(train_data, padded_sents)
    #print("Size vocabulary: " + str(len(model.vocab)))
    #print("Model counts: " + str(model.counts))
    #print("Counts av: " + str(model.counts['av']))
    #print("Counts rua: " + str(model.counts['rua']))

    n = 3
    model3_k = KneserNeyInterpolated(n)
    model3_w = WittenBellInterpolated(n)
    model3_n = Laplace(n)
    train_data3, padded_sents3 = padded_everygram_pipeline(n, tokenized_text)
    model3_k.fit(train_data3, padded_sents3)
    train_data3, padded_sents3 = padded_everygram_pipeline(n, tokenized_text)
    model3_w.fit(train_data3, padded_sents3)
    train_data3, padded_sents3 = padded_everygram_pipeline(n, tokenized_text)
    model3_n.fit(train_data3, padded_sents3)
    print("Size vocabulary: " + str(len(model3_k.vocab)))
    print("Model counts: " + str(model3_k.counts))
    print("Size vocabulary: " + str(len(model3_w.vocab)))
    print("Model counts: " + str(model3_w.counts))
    print("Size vocabulary: " + str(len(model3_n.vocab)))
    print("Model counts: " + str(model3_n.counts))
    #print("Counts av: " + str(model3.counts['av']))
    #print("Counts rua: " + str(model3.counts['rua']))

    with open("LMKneserBigramModel", "wb") as lm:
        pickle.dump(model_k, lm, protocol=2)
    with open("LMKneserTrigramModel", "wb") as lm:
        pickle.dump(model3_k, lm, protocol=2)

    with open("LMWittenBigramModel", "wb") as lm:
        pickle.dump(model_w, lm, protocol=2)
    with open("LMWittenTrigramModel", "wb") as lm:
        pickle.dump(model3_w, lm, protocol=2)

    with open("LMBigramModel", "wb") as lm:
        pickle.dump(model_n, lm, protocol=2)
    with open("LMTrigramModel", "wb") as lm:
        pickle.dump(model3_n, lm, protocol=2)


    with open("LMNumTokensDic", "wb") as lm:
        pickle.dump(num_tokens_dic, lm, protocol=2)

    #print(model.vocab.lookup(tokenized_text[0]))

    #tokenized_text = [list(map(str.lower, word_tokenize(sent))) 
    #                  for sent in sent_tokenize(' Estrada Municipal 1391 ola .')]
    #print(model.vocab.lookup(tokenized_text))
    #print(model.score(tokenized_text))

def calc_model_score(model, model3, phrase, num):
    count = 1

    if num == 2:
        for i in range(1, len(phrase)):
            #nltk_log_file.write(str(phrase[i]) + " | " + str(phrase[i-1]) + "\n")
            #nltk_log_file.write("Bigram: Count of " + str(phrase[i]) + " in model -> " + str(model.counts[phrase[i]]) + "\n")
            #nltk_log_file.write("Bigram: Count of " + str(phrase[i-1]) + " in model -> " + str(model.counts[phrase[i-1]]) + "\n")
            count *= model.score(phrase[i], [phrase[i-1]])
            #nltk_log_file.write("Bigram: Count multiplied: " + str(phrase[i]) + " | " + str(phrase[i-1]) + " -> " + str(model.score(phrase[i], [phrase[i-1]])) + "\n")
            #nltk_log_file.write("\n")

        #nltk_log_file.write("\nBigram_FinalValue: " + str(count) + "\n\n")
            
    elif num == 3:
        for i in range(2, len(phrase)-1):
            #nltk_log_file.write(str(phrase[i]) + " | " + str(phrase[i-2]) + " " + str(phrase[i-1]) + "\n")
            #nltk_log_file.write("Trigram: Count of: " + str(phrase[i]) + " -> " + str(model.counts[phrase[i]]) + "\n")
            #nltk_log_file.write("Trigram: Count of: " + str(phrase[i-1]) + " -> " + str(model.counts[phrase[i-1]]) + "\n")
            #nltk_log_file.write("Trigram: Count of: " + str(phrase[i-2]) + " -> " + str(model.counts[phrase[i-2]]) + "\n")
            count *= model3.score(phrase[i], [phrase[i-2], phrase[i-1]])
            #nltk_log_file.write("Trigram: Count multiplied: " + str(phrase[i]) + ", [" + str(phrase[i-2]) + ", " + str(phrase[i-1]) + "] -> " + str(model3.score(phrase[i], [phrase[i-2], phrase[i-1]])) + "\n")
            #nltk_log_file.write("\n")

        #nltk_log_file.write("\nTrigram_FinalValue: " + str(count) + "\n\n")

    return count

def testLMs(var, thresholds):

    if var == "kneser":
        with open("LMKneserBigramModel", "rb") as g:
            model = pickle.load(g)
        with open("LMKneserTrigramModel", "rb") as g:
            model3 = pickle.load(g)
        #nltk_log_file = open("nltk_log_file_kneser.txt", "w", encoding="utf-8")
        resFile = open("../results/LMResults_KneserNey.txt", "w", encoding="utf-8")


    elif var == "witten":
        with open("LMWittenBigramModel", "rb") as g:
            model = pickle.load(g)
        with open("LMWittenTrigramModel", "rb") as g:
            model3 = pickle.load(g)
        #nltk_log_file = open("nltk_log_file_witten.txt", "w", encoding="utf-8")
        resFile = open("../results/LMResults_WittenBell.txt", "w", encoding="utf-8")


    elif var == "laplace":
        with open("LMBigramModel", "rb") as g:
            model = pickle.load(g)
        with open("LMTrigramModel", "rb") as g:
            model3 = pickle.load(g)
        #nltk_log_file = open("nltk_log_file_laplace.txt", "w", encoding="utf-8")
        resFile = open("../results/LMResults_Laplace.txt", "w", encoding="utf-8")



    with open("LMNumTokensDic", "rb") as g:
        num_tokens_dic = pickle.load(g)



    for threshold in thresholds:
        resFile.write("Threshold: " + str(threshold) + "\n")

        score_bimin = 100
        score_bimax = -100
        scores_bi = []
        score_trimin = 100
        score_trimax = -100
        scores_tri = []
        unks = 0

        # Save all precisions to divide them later
        precisions = []
        recalls = []
        fmeasures = []
        all_precision_scores = []
        all_precision_under_scores = []
        all_recall_scores = []
        all_recall_under_scores = []
        all_fmeasure_top_scores = []
        all_fmeasure_under_scores = []

        precisions3 = []
        recalls3 = []
        fmeasures3 = []
        all_precision_scores3 = []
        all_precision_under_scores3 = []
        all_recall_scores3 = []
        all_recall_under_scores3 = []
        all_fmeasure_top_scores3 = []
        all_fmeasure_under_scores3 = []


        entities = ["sharisushibar", "restaurante-sol-e-mar", "restaurantedoclubenavaldelisboa", "jardimdosentidos", "aquariumcoimbra", \
                    "gaviaonovo", "8healthlounge", "restaurantedonpedro1", "sakeportugal", "clinicadentariajardimdosarcos", \
                    "hennessys-pub", "doiscorvos", "salpoente", "eduardomerino", "centromedicodetondela", "vetdinha", "dnuno", \
                    "restauranteacantarinha", "cufra", "palaciochiado", "restaurantelilys", "restaurantemarradas", "voltaemeia", \
                    "INATEL", "farmaciacodeco", "povolisboa", "espacodoluto", "caldeirasevulcoes", "ladobcafe", "ovitral" \
                    ]


        for fileEn in entities:
            filename = "../annotated/rules1_" + fileEn + ".txt"
            entity_file = open(filename, "r", encoding="utf-8")
            
            eval_dic = {"Morada 1 LM 1": 0, "Morada 1 LM 0": 0, "Morada 0 LM 1": 0, "Morada 0 LM 0": 0}
            eval_dic3 = {"Morada 1 LM 1": 0, "Morada 1 LM 0": 0, "Morada 0 LM 1": 0, "Morada 0 LM 0": 0}

            for line in entity_file:
                # If the rules identified a possible hour or phone candidate the system will ignore it 
                # since the language model will only classify the address attribute values found by the
                # regular expressions with the tag -> Morada!
                if not re.search("ADDRESS1:", line):
                    continue

                # If line is empty skip it
                if line == "" or line == "\n":
                    continue

                # If line identifies a file then skip it as it is irrelevant for the classifier 
                if re.search(r'\bfile', line):
                    continue
                    

                # Split the line and fetch annotation value
                line_split = line.split()
                attribute_type = int(line_split[-1])
                line = " ".join(line_split[1:-1])
                # Replace all numbers by <NUM> token so they match as a single entity
                # so as to not give too many unknown words in the LM
                line = re.sub(r'\b[0-9]+\b', "NUM", line)
                #print(attribute_type)

                # Check how many unknown words we have
                for word in model.vocab.lookup(word_tokenize(line)):
                    if word == "<UNK>":
                        unks += 1


                # Calculate the bigram score of the phrase
                phrase = list(pad_both_ends(word_tokenize(line.lower()), n=2))
                #nltk_log_file.write(str(phrase) + "\n")
                #nltk_log_file.write(str(model.vocab.lookup(phrase)) + "\n")
                score_model = calc_model_score(model, model3, phrase, 2)
                #nltk_log_file.write("\nSCORE BI: " + str(score_model) + "\n")
                
                # Calculate the trigram score of the phrase
                phrase3 = list(pad_both_ends(word_tokenize(line.lower()), n=3))
                #nltk_log_file.write(str(phrase3) + "\n")
                #nltk_log_file.write(str(model3.vocab.lookup(phrase3)) + "\n")
                score_model3 = calc_model_score(model, model3, phrase, 3)
                #nltk_log_file.write("\nSCORE TRI: " + str(score_model3) + "\n")

                # If the phrases length has never been seen make it a rare existing length
                num = len(phrase)-2
                if num not in num_tokens_dic:
                    num = 14
                
                # Smooth the score by taking into account how many addresses in the training corpus
                # had the same length
                score_bi = score_model*num_tokens_dic[num]/sum(num_tokens_dic.values())
                score_tri = score_model3*num_tokens_dic[num]/sum(num_tokens_dic.values())
                #nltk_log_file.write("Score: " + str(score_bi) + " " + str(score_tri) + "\n\n")

                # Calculate the min and max score for all bi/tri-grams
                if score_bi - score_bimin < 0:
                    score_bimin = score_bi
                if score_bi - score_bimax > 0:
                    score_bimax = score_bi
                if score_tri - score_trimin < 0:
                    score_trimin = score_tri
                if score_tri - score_trimax > 0:
                    score_trimax = score_tri
                scores_bi.append(score_bi)
                scores_tri.append(score_tri)

                # If the score is above a threshold then check the annotation to verify if it is a match
                # otherwise its not a match
                if score_bi > threshold:
                    if re.search("1", str(attribute_type)):
                        eval_dic["Morada 1 LM 1"] += 1
                    else:
                        eval_dic["Morada 0 LM 1"] += 1
                else:
                    if re.search("1", str(attribute_type)):
                        eval_dic["Morada 1 LM 0"] += 1                    
                    else:
                        eval_dic["Morada 0 LM 0"] += 1

                if score_tri > threshold:
                    if re.search("1", str(attribute_type)):
                        eval_dic3["Morada 1 LM 1"] += 1
                    else:
                        eval_dic3["Morada 0 LM 1"] += 1
                else:
                    if re.search("1", str(attribute_type)):
                        eval_dic3["Morada 1 LM 0"] += 1                    
                    else:
                        eval_dic3["Morada 0 LM 0"] += 1

            #resFile.write("\n")
            #nltk_log_file.write("\n\n")
            #nltk_log_file.write("Unknown words: " + str(unks) + "\n")
            #nltk_log_file.write("Min score: " + str(score_bimin) + " " + str(score_trimin) + "\n")
            #nltk_log_file.write("Max score: " + str(score_bimax) + " " + str(score_trimax) + "\n")

            # Print metrics related to the address attribute by file for bigrams
            score = eval_dic["Morada 1 LM 1"]
            all_precision_scores.append(score)
            divide = eval_dic["Morada 1 LM 1"] + eval_dic["Morada 0 LM 1"]
            all_precision_under_scores.append(divide)
            if divide == 0:
                score = 0
            else:
                score /= eval_dic["Morada 1 LM 1"] + eval_dic["Morada 0 LM 1"]
            #nltk_log_file.write("Precision: " + str(score) + "\n")
            precisions.append(score)
            precision = score

            score = eval_dic["Morada 1 LM 1"]
            all_recall_scores.append(score)
            divide = eval_dic["Morada 1 LM 1"] + eval_dic["Morada 1 LM 0"]
            all_recall_under_scores.append(divide)
            if divide == 0:
                score = 0
            else:
                score /= eval_dic["Morada 1 LM 1"] + eval_dic["Morada 1 LM 0"]
            #nltk_log_file.write("Recall: " + str(score) + "\n")
            recalls.append(score)
            recall = score


            score = 2*precision*recall
            all_fmeasure_top_scores.append(score)
            divide = precision+recall
            all_fmeasure_under_scores.append(divide)
            if divide == 0:
                score = 0
            else:
                score /= divide
            fmeasures.append(score)


            # score = eval_dic["Morada 1 LM 1"] + eval_dic["Morada 0 LM 0"]
            # divide = eval_dic["Morada 1 LM 1"] + eval_dic["Morada 0 LM 0"] + eval_dic["Morada 1 LM 0"] + eval_dic["Morada 0 LM 1"]
            # if divide == 0:
            #     score = 0
            # else:
            #     score /= eval_dic["Morada 1 LM 1"] + eval_dic["Morada 0 LM 0"] + eval_dic["Morada 1 LM 0"] + eval_dic["Morada 0 LM 1"]
            # #nltk_log_file.write("Accuracy: " + str(score) + "\n")
            # #nltk_log_file.write(str(eval_dic) + "\n")

            # entity_file.close()

            # Print metrics related to the address attribute by file for Trigrams
            score = eval_dic3["Morada 1 LM 1"]
            all_precision_scores3.append(score)
            divide = eval_dic3["Morada 1 LM 1"] + eval_dic3["Morada 0 LM 1"]
            all_precision_under_scores3.append(divide)
            if divide == 0:
                score = 0
            else:
                score /= eval_dic3["Morada 1 LM 1"] + eval_dic3["Morada 0 LM 1"]
            #nltk_log_file.write("Precision: " + str(score) + "\n")
            precisions3.append(score)
            precision3 = score

            score = eval_dic3["Morada 1 LM 1"]
            all_recall_scores3.append(score)
            divide = eval_dic3["Morada 1 LM 1"] + eval_dic3["Morada 1 LM 0"]
            all_recall_under_scores3.append(divide)
            if divide == 0:
                score = 0
            else:
                score /= eval_dic3["Morada 1 LM 1"] + eval_dic3["Morada 1 LM 0"]
            #nltk_log_file.write("Recall: " + str(score) + "\n")
            recalls3.append(score)
            recall3 = score


            score = 2*precision3*recall3
            all_fmeasure_top_scores3.append(score)
            divide = precision3+recall3
            all_fmeasure_under_scores3.append(divide)
            if divide == 0:
                score = 0
            else:
                score /= divide
            fmeasures3.append(score)


        #BIGRAMS
        resFile.write("Bigrams\n")
        resFile.write("Macro Average Precision: " + str(sum(precisions)/len(precisions)) + "\n")
        if sum(all_precision_under_scores) == 0:
            micro = 0
        else:
            micro = sum(all_precision_scores)/sum(all_precision_under_scores)
        resFile.write("Micro Average Precision: " + str(micro) + "\n")

        resFile.write("Macro Average Recall: " + str(sum(recalls)/len(recalls)) + "\n")
        if sum(all_recall_under_scores) == 0:
            micro = 0
        else:
            micro = sum(all_recall_scores)/sum(all_recall_under_scores)
        resFile.write("Micro Average Recall: " + str(micro) + "\n")

        resFile.write("Macro Average F-Measure: " + str(sum(fmeasures)/len(fmeasures)) + "\n")
        if sum(all_fmeasure_under_scores) == 0:
            micro = 0
        else:
            micro = sum(all_fmeasure_top_scores)/sum(all_fmeasure_under_scores)
        resFile.write("Micro Average F-Measure: " + str(micro) + "\n\n")



        # TRIGRAMS
        resFile.write("Trigrams\n")
        resFile.write("Macro Average Precision: " + str(sum(precisions3)/len(precisions3)) + "\n")
        if sum(all_precision_under_scores3) == 0:
            micro = 0
        else:
            micro = sum(all_precision_scores3)/sum(all_precision_under_scores3)
        resFile.write("Micro Average Precision: " + str(micro) + "\n")

        resFile.write("Macro Average Recall: " + str(sum(recalls3)/len(recalls3)) + "\n")
        if sum(all_recall_under_scores3) == 0:
            micro = 0
        else:
            micro = sum(all_recall_scores3)/sum(all_recall_under_scores3)
        resFile.write("Micro Average Recall: " + str(micro) + "\n")

        resFile.write("Macro Average F-Measure: " + str(sum(fmeasures3)/len(fmeasures3)) + "\n")
        if sum(all_fmeasure_under_scores3) == 0:
            micro = 0
        else:
            micro = sum(all_fmeasure_top_scores3)/sum(all_fmeasure_under_scores3)
        resFile.write("Micro Average F-Measure: " + str(micro) + "\n\n")
    

    #nltk_log_file.close()
    resFile.close()


def realtestLMs(var, threshold):
    try:
        if var == "kneser":
            with open("LMKneserBigramModel", "rb") as g:
                model = pickle.load(g)
            with open("LMKneserTrigramModel", "rb") as g:
                model3 = pickle.load(g)
            nltk_log_file = open("../results/User_LMResults_Bi_Kneser.txt", "w", encoding="utf-8")
            nltk_log_file3 = open("../results/User_LMResults_Tri_Kneser.txt", "w", encoding="utf-8")


        elif var == "witten":
            with open("LMWittenBigramModel", "rb") as g:
                model = pickle.load(g)
            with open("LMWittenTrigramModel", "rb") as g:
                model3 = pickle.load(g)
            nltk_log_file = open("../results/User_LMResults_Bi_Witten.txt", "w", encoding="utf-8")
            nltk_log_file3 = open("../results/User_LMResults_Tri_Witten.txt", "w", encoding="utf-8")


        elif var == "laplace":
            with open("LMBigramModel", "rb") as g:
                model = pickle.load(g)
            with open("LMTrigramModel", "rb") as g:
                model3 = pickle.load(g)
            nltk_log_file = open("../results/User_LMResults_Bi_Laplace.txt", "w", encoding="utf-8")
            nltk_log_file3 = open("../results/User_LMResults_Tri_Laplace.txt", "w", encoding="utf-8")


        with open("LMNumTokensDic", "rb") as g:
            num_tokens_dic = pickle.load(g)

        with open("../UserWebsites", "rb") as f:
            user_list = pickle.load(f)          

        entities = []
        for element in user_list:
            split_url = urlparse(element)
            url = split_url.netloc
            url = re.sub("www.", "", url)
            url = re.sub(".com", "", url)
            url = re.sub(".pt", "", url)
            url = re.sub(".net", "", url)
            entities.append(url)


        for fileEn in entities:
            filename = "../user/entities/rules1_" + fileEn + ".txt"
            entity_file = open(filename, "r", encoding="utf-8")
            nltk_log_file.write(fileEn + "\n")
            
            for line in entity_file:
                original = line
                
                # If the rules identified a possible hour or phone candidate the system will ignore it 
                # since the language model will only classify the address attribute values found by the
                # regular expressions with the tag -> Morada!
                if not re.search("ADDRESS1: ", line):
                    continue

                # If line is empty skip it
                if line == "" or line == "\n":
                    continue

                # If line identifies a file then skip it as it is irrelevant for the classifier 
                if re.search(r'\bfile', line):
                    continue
                    
                    
                # Replace all numbers by <NUM> token so they match as a single entity
                # so as to not give too many unknown words in the LM
                line = re.sub(r'\b[0-9]+\b', "NUM", line)
                #print(attribute_type)


                # Calculate the bigram score of the phrase
                phrase = list(pad_both_ends(word_tokenize(line.lower()), n=2))
                #nltk_log_file.write(str(phrase) + "\n")
                #nltk_log_file.write(str(model.vocab.lookup(phrase)) + "\n")
                score_model = calc_model_score(model, model3, phrase, 2)
                #nltk_log_file.write("\nSCORE BI: " + str(score_model) + "\n")
                
                # Calculate the trigram score of the phrase
                phrase3 = list(pad_both_ends(word_tokenize(line.lower()), n=3))
                #nltk_log_file.write(str(phrase3) + "\n")
                #nltk_log_file.write(str(model3.vocab.lookup(phrase3)) + "\n")
                score_model3 = calc_model_score(model, model3, phrase, 3)
                #nltk_log_file.write("\nSCORE TRI: " + str(score_model3) + "\n")

                # If the phrases length has never been seen make it a rare existing length
                num = len(phrase)-2
                if num not in num_tokens_dic:
                    num = 14
                
                # Smooth the score by taking into account how many addresses in the training corpus
                # had the same length
                score_bi = score_model*num_tokens_dic[num]/sum(num_tokens_dic.values())
                score_tri = score_model3*num_tokens_dic[num]/sum(num_tokens_dic.values())


                # If the score is above a threshold then check the annotation to verify if it is a match
                # otherwise its not a match
                if score_bi > threshold:
                    nltk_log_file.write(original + "\n")
                if score_tri > threshold:
                    nltk_log_file3.write(original + "\n")

    except Exception as e:
        print("Exception: " + str(e))


if sys.argv[1] == "train_lms":
    trainLMs()

elif sys.argv[1] == "test_lm_kneser":
    print("Test LM Kneser")
    testLMs("kneser", [10**-30, 10**-50, 10**-80, 10**-100])

elif sys.argv[1] == "test_lm_witten":
    print("Test LM Witten")
    testLMs("witten", [10**-30, 10**-50, 10**-80, 10**-100])
    
elif sys.argv[1] == "test_lm_laplace":
    print("Test LM Laplace")
    testLMs("laplace", [10**-30, 10**-50, 10**-80, 10**-100])

elif sys.argv[1] == "test_lms":
    realtestLMs("kneser", 10**-100)
    realtestLMs("witten", 10**-100)
    realtestLMs("laplace", 10**-100)




end = time.time()
print(end - start)

