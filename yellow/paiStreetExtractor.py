#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Regular expressions library 
import re
from nltk import word_tokenize

# List that will contain all components of an address if they exist
address = ""

# Files to save all portugal addresses normalized and unormalized
addresses = open("paiAddresses.txt", "r", encoding='utf-8')
streets = open("paiStreets.txt", "w", encoding='utf-8')

streets_dic = {}

# Iteration through document nodes
for line in addresses:      

        #line = re.sub("[\.º/(),]*", "", line)
        #line = re.sub("[Ll][Oo][Tt][Ee]", "", line)
        #line = re.sub("nº", "", line)

        # identifica um codigo postal e cidade aseguir
        # if re.search("[0-9]+[\- ]*[0-9]+[\- A-Za-zãáàâéèêíìîóòõôúùûÃÁÀÂÉÈÊÍÌÎÓÒÕÔÚÙÛçÇ]+\n$", line):
        #         line = re.sub("[0-9]+[\- ]*[0-9]+[\- A-Za-zãáàâéèêíìîóòõôúùûÃÁÀÂÉÈÊÍÌÎÓÒÕÔÚÙÛçÇ]+\n$", "\n", line)
        #         streets.write("line postal + word: " + line)

        # # identifica um numero de porta e localidade
        # elif re.search("[0-9\-/]+[\- A-Za-zãáàâéèêíìîóòõôúùûÃÁÀÂÉÈÊÍÌÎÓÒÕÔÚÙÛçÇ]+\n$", line):
        #         line = re.sub("[0-9\-/]+[\- A-Za-zãáàâéèêíìîóòõôúùûÃÁÀÂÉÈÊÍÌÎÓÒÕÔÚÙÛçÇ]+\n$", "\n", line)
        #         streets.write("line num + word: " + line)

        # # identifica um numero de porta com letra
        # elif re.search("[0-9]+[\-][\- A-Za-zãáàâéèêíìîóòõôúùûÃÁÀÂÉÈÊÍÌÎÓÒÕÔÚÙÛçÇ]+\n$", line):
        #         line = re.sub("[0-9]+[\-][\- A-Za-zãáàâéèêíìîóòõôúùûÃÁÀÂÉÈÊÍÌÎÓÒÕÔÚÙÛçÇ]+\n$", "\n", line)
        #         streets.write("line num with letter: " + line)

        # # identifica um numero de porta
        # elif re.search("[0-9 ]+\n$", line):
        #         line = re.sub("[0-9 ]+\n$", "\n", line)
        #         streets.write("line num: " + line)

        #else:
                #streets.write("EXCEPTION")

        line = re.sub("\.", "", line)
        line = re.sub("[ ]+\n$", "\n", line)
        

        # If line is repeated skip it
        if line in streets_dic:
                continue   
        else:  
                streets_dic[line] = 1  

        streets.write(line)
	

# Close used files
addresses.close()
streets.close()

streets = open("paiStreets.txt", "r", encoding="utf-8")
lines = streets.readlines()
streets.close()

streets = open("paiStreets.txt", "w", encoding="utf-8")    

for i in range(0, len(lines)-1):
        if lines[i] != "\n":
                streets.write(lines[i])

streets.close()      

streets = open("paiStreets.txt", "r", encoding="utf-8")   
num_words = []

for line in streets:
        if len(word_tokenize(line)) == 0:
                continue
        num_words.append(len(word_tokenize(line)))


print("Number of streets analyzed: ", len(num_words))
print("Average word length: ", sum(num_words)/len(num_words))
print("Min word length: ", min(num_words))
print("Max word length: ", max(num_words))   

streets.close()
