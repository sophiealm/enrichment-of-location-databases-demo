#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
import re
from urllib.parse import urlparse
from scrapy import Selector

class WebsitesSpider(scrapy.Spider):
        name = "yellow"
        download_delay = 5.0
        # Handle HTTP Redirects
        handle_httpstatus_list = [302]

        # Keeps track of number of files
        count = 0
                
        start_urls = [  "http://www.pai.pt/cafes",
                        "https://www.pai.pt/bares",
                        "https://www.pai.pt/pastelarias-e-confeitarias",
                        "https://www.pai.pt/restaurantes",
                        "https://www.pai.pt/casas-de-cha",
                        "https://www.pai.pt/padarias",
                        "https://www.pai.pt/gelatarias",
                        "https://www.pai.pt/discotecas",
                        "https://www.pai.pt/talhos",
                        "https://www.pai.pt/mercados",
                        "https://www.pai.pt/mercearias",
                        "https://www.pai.pt/supermercados",
                        "https://www.pai.pt/peixarias",
                        "https://www.pai.pt/gastronomia-e-vinhos",
                        "https://www.pai.pt/hospitais",
                        "https://www.pai.pt/centros-de-saude",
                        "https://www.pai.pt/analises-clinicas-e-exames",
                        "https://www.pai.pt/clinicas-e-especialidades-medicas",
                        "https://www.pai.pt/terapias-e-medicinas-alternativas",
                        "https://www.pai.pt/centros-de-enfermagem",
                        "https://www.pai.pt/lares-de-idosos",
                        "https://www.pai.pt/apoio-domiciliario",
                        "https://www.pai.pt/emergencia-medica",
                        "https://www.pai.pt/farmacias-e-parafarmacias",
                        "https://www.pai.pt/produtos-naturais-e-dieteticos",
                        "https://www.pai.pt/cabeleireiros",
                        "https://www.pai.pt/barbeiros",
                        "https://www.pai.pt/centros-de-estetica",
                        "https://www.pai.pt/spas-e-massagens",
                        "https://www.pai.pt/opticas-e-oculistas",
                        "https://www.pai.pt/aparelhos-auditivos",
                        "https://www.pai.pt/tatuagens",
                        "https://www.pai.pt/perfumarias",
                        "https://www.pai.pt/marketing-e-comunicacao",
                        "https://www.pai.pt/centros-de-copias",
                        "https://www.pai.pt/it-informatica",
                        "https://www.pai.pt/gestao-e-servicos-financeiros",
                        "https://www.pai.pt/servicos-legais",
                        "https://www.pai.pt/limpezas",
                        "https://www.pai.pt/servicos-de-seguranca",
                        "https://www.pai.pt/reparacoes-e-assistencia-tecnica",
                        "https://www.pai.pt/antenas-e-satelites",
                        "https://www.pai.pt/desinfestacoes-e-fumigacoes",
                        "https://www.pai.pt/mudancas",
                        "https://www.pai.pt/distribuidores-de-gas",
                        "https://www.pai.pt/funerais",
                        "https://www.pai.pt/canalizadores-e-picheleiros",
                        "https://www.pai.pt/eletricistas",
                        "https://www.pai.pt/desentupimentos",
                        "https://www.pai.pt/costureiras-e-modistas",
                        "https://www.pai.pt/alfaiates",
                        "https://www.pai.pt/restauros",
                        "https://www.pai.pt/seguros",
                        "https://www.pai.pt/imobiliarias",
                        "https://www.pai.pt/avaliadores",
                        "https://www.pai.pt/certificacao",
                        "https://www.pai.pt/chaveiros",
                        "https://www.pai.pt/pintores",
                        "https://www.pai.pt/carpinteiros",
                        "https://www.pai.pt/jardineiros",
                        "https://www.pai.pt/administracao-de-condominios",
                        "https://www.pai.pt/penhores",
                        "https://www.pai.pt/babysitters",
                        "https://www.pai.pt/funileiros",
                        "https://www.pai.pt/estofadores",
                        "https://www.pai.pt/mecanicos",
                        "https://www.pai.pt/servicos-automovel",
                        "https://www.pai.pt/servicos-de-traducao",
                        "https://www.pai.pt/modelismo",
                        "https://www.pai.pt/servicos-de-consultoria",
                        "https://www.pai.pt/franchising",
                        "https://www.pai.pt/floristas",
                        "https://www.pai.pt/jardim-e-espaco-exterior",
                        "https://www.pai.pt/mobiliario",
                        "https://www.pai.pt/pavimentos-e-revestimentos",
                        "https://www.pai.pt/decoracao",
                        "https://www.pai.pt/aquecimento-e-climatizacao",
                        "https://www.pai.pt/bricolage",
                        "https://www.pai.pt/piscinas-e-acessorios",
                        "https://www.pai.pt/casa-de-banho",
                        "https://www.pai.pt/cozinha",
                        "https://www.pai.pt/eletrodomesticos",
                        "https://www.pai.pt/camas-e-colchoes",
                        "https://www.pai.pt/sofas-e-cadeiroes",
                        "https://www.pai.pt/iluminacao",
                        "https://www.pai.pt/texteis-e-tapetes",
                        "https://www.pai.pt/video-e-fotografia",
                        "https://www.pai.pt/tv-e-som",
                        "https://www.pai.pt/informatica-e-acessorios",
                        "https://www.pai.pt/telecomunicacoes-e-acessorios",
                        "https://www.pai.pt/relojoarias",
                        "https://www.pai.pt/ourivesarias",
                        "https://www.pai.pt/joalharias",
                        "https://www.pai.pt/bijuterias",
                        "https://www.pai.pt/lojas-de-roupa",
                        "https://www.pai.pt/lojas-de-acessorios",
                        "https://www.pai.pt/sex-shops",
                        "https://www.pai.pt/lingerie-e-roupa-interior",
                        "https://www.pai.pt/sapatarias",
                        "https://www.pai.pt/puericultura",
                        "https://www.pai.pt/trajes-e-fardas",
                        "https://www.pai.pt/fornecedores-de-calcado",
                        "https://www.pai.pt/fornecedores-de-moda",
                        "https://www.pai.pt/pirotecnia",
                        "https://www.pai.pt/biotecnologia",
                        "https://www.pai.pt/fornecedores-de-lojas-e-comercio",
                        "https://www.pai.pt/construcao-civil",
                        "https://www.pai.pt/material-de-construcao",
                        "https://www.pai.pt/equipamentos-para-construcao",
                        "https://www.pai.pt/arquitetos",
                        "https://www.pai.pt/engenharia",
                        "https://www.pai.pt/metalurgia",
                        "https://www.pai.pt/metalomecanica",
                        "https://www.pai.pt/coberturas",
                        "https://www.pai.pt/domotica",
                        "https://www.pai.pt/vidrarias",
                        "https://www.pai.pt/material-industrial",
                        "https://www.pai.pt/equipamentos-industriais",
                        "https://www.pai.pt/fornecedores-saude",
                        "https://www.pai.pt/analises-quimicas",
                        "https://www.pai.pt/serralharias",
                        "https://www.pai.pt/pedreiras",
                        "https://www.pai.pt/industria",
                        "https://www.pai.pt/servicos-industriais",
                        "https://www.pai.pt/equipamento-hoteleiro",
                        "https://www.pai.pt/fornecedores-de-limpezas",
                        "https://www.pai.pt/sistemas-de-aspiracao-central",
                        "https://www.pai.pt/sistemas-de-aquecimento",
                        "https://www.pai.pt/seguranca-e-qualidade",
                        "https://www.pai.pt/elevadores",
                        "https://www.pai.pt/plataformas-elevatorias",
                        "https://www.pai.pt/tratamento-de-residuos",
                        "https://www.pai.pt/energia",
                        "https://www.pai.pt/electricidade",
                        "https://www.pai.pt/gas",
                        "https://www.pai.pt/combustiveis",
                        "https://www.pai.pt/protecao-do-ambiente",
                        "https://www.pai.pt/agua-e-canalizacao",
                        "https://www.pai.pt/pavimentos-industriais",
                        "https://www.pai.pt/pavimentacao-de-estradas",
                        "https://www.pai.pt/mercadorias-e-transporte",
                        "https://www.pai.pt/correios",
                        "https://www.pai.pt/agricultura-e-horticultura",
                        "https://www.pai.pt/pecuaria",
                        "https://www.pai.pt/floricultores",
                        "https://www.pai.pt/aviarios",
                        "https://www.pai.pt/apicultura",
                        "https://www.pai.pt/embalagens",
                        "https://www.pai.pt/fornecedores-de-plastico",
                        "https://www.pai.pt/ventilacao",
                        "https://www.pai.pt/materia-prima",
                        "https://www.pai.pt/fornecedores-de-papel",
                        "https://www.pai.pt/maquinas-de-vending",
                        "https://www.pai.pt/servicos-de-navios",
                        "https://www.pai.pt/portos-e-marinas",
                        "https://www.pai.pt/madeiras",
                        "https://www.pai.pt/fabricantes-de-automoveis",
                        "https://www.pai.pt/fornecedores-de-alimentacao",
                        "https://www.pai.pt/electronica",
                        "https://www.pai.pt/fornecedores-de-brinquedos",
                        "https://www.pai.pt/equipamento-grafico",
                        "https://www.pai.pt/baterias",
                        "https://www.pai.pt/pesca"
                        # PAREI ANTES DAS VIAGENS E ALOJAMENTO
                        ]

                #yield scrapy.Request(url="", encoding='utf-8', dont_filter=True, callback=self.parse)


        # Function that returns True if file is a bad file
        def bad_file(self, page):
                return re.search('.jpg', page.lower()) or re.search('.pdf', page.lower()) \
                                        or re.search('mailto:', page.lower()) or re.search('tel:', page.lower()) \
                                        or re.search('maps', page.lower()) or re.search('youtube', page.lower()) \
                                        or re.search('javascript', page.lower()) or re.search('.png', page.lower()) \
                                        or re.search('facebook', page.lower()) or re.search('.gif', page.lower()) \
                                        or re.search('.php', page.lower())


        def parse(self, response):
            # Create a log file to check errors in parsing function of scrapy
            with open('pai_log_file.txt', 'a') as log:
                try:
                        self.count += 1

                        response_url = response.url
                        log.write("\nCURRENT_URL: " + response_url + "\n")
                                
                        # Check next page of this category
                        # Capture all links in HTML <a> tags in order to follow possible relevant links
                        #next_page = response.css('.gr__pai_pt .production searches index #search #results \
                        #    .grid-container .search-results .grid-x grid-margin-x .cell medium-9 .card card--result \
                        #    .cell small-8 medium-8 .card-section .grid-x grid-margin-x .cell large-4 \
                        #    .cell card-address').getall()
                        current_p_addresses = response.css('.card-address::text').getall()
                        log.write(str(current_p_addresses) + "\n")
                        self.log("\n\n\n\n")

                        # Open file to save all pai addresses
                        addresses = open('paiAddresses.txt', 'a')
                        
                        for page in current_p_addresses:
                            log.write("Address: " + page + "\n")
                            addresses.write(page + "\n")

                        addresses.close()

                        next_page = response.css('.next a::attr(href)').get()
                        log.write("NEXT_PAGE: " + str(next_page) + "\n")
                        self.log("\n\n\n\n")

                        if next_page:

                            # If page is pdf/etc ignore it
                            if self.bad_file(next_page):
                                log.write("BAD FILE TYPE\n")
                            
                            else:
                                base_page = response.urljoin(next_page)
                                self.log("\n\nPAGE TO BE SOUGHT: " + base_page)
                                    
                                yield scrapy.Request(base_page, encoding='utf-8', dont_filter=True, callback=self.parse)


                except Exception as e:
                    log.write("\nEXCEPTION:\n")
                    log.write(str(response.url) + "\n")
                    log.write("e: " + str(e))
                    log.write("\n")
