#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET
import re
import pickle

# CONVERT FILE TO ELEMENT OBJECT
tree = ET.parse('amenity_v8_04.osm.xml')
#tree = ET.parse('restaurant.osm.xml')
#tree = ET.parse('dinner.osm.xml')
root = tree.getroot()
#print(root.tag)

# List that will contain all museums needed
lst_entities = []
no_dup = {}
all_entities = {}

# Iteration through document
for node in root.iter('node'):
        #print(node.attrib)

        # Variable that will check if all atributes are present
        hasAll = {1: 0, 2: 0, 3: 0, 4: 0}
        # Travel all tags of a node
        for child in node:
                # Check if tag is any kind of phone
                if child.get("k") == "phone" or child.get("k") == "contact:phone":
                        hasAll[1] += 1
                # Check if tag is any kind of opening hours
                if child.get("k") == "opening_hours":
                        hasAll[2] += 1
                # Check if tag is any kind of valid address
                if child.get("k") == "addr:street" or child.get("k") == "addr:city" or child.get("k") == "addr:housenumber" or child.get("k") == "addr:postcode":
                        hasAll[3] += 1
                # Check if tag is a website and website is not a facebook webpage
                if child.get("k") == "website":
                        x = re.search("facebook", child.get("v").lower())
                        if not (x):
                                hasAll[4] += 1
        #print(str(hasAll))
        # Check if node has all attributes
        if hasAll[1] > 0 and hasAll[2] > 0 and hasAll[3] > 0 and hasAll[4] > 0:
                # Avoid duplicates
                for child in node:
                    if child.get("k") == "website" and child.get("v") not in no_dup.keys(): 
                        all_entities[child.get("v")] = {"name":[], "street":[], "postal":[], "city":[], "house":[], "phone":[], "opening_hours":[]}
                        no_dup[child.get("v")] = 1
                        lst_entities.append(node)

        
# COLLECT ALL RELEVANT WEBSITES TO CRAWL

# File to collect all websites
websites = open("websites.txt", "w")
websitesNoFace = 0
name = ""

# Print all relevant nodes
for node in lst_entities:
    for child in node:
        if child.get("k") == "website":
            name = child.get("v")
            print(name)
            x = re.search("facebook.com", child.get("v").lower())
            if not (x):
                websitesNoFace +=1
                websites.write(child.get("v") + "\n")
    
    for child in node:
            if child.get("k") == "addr:street":
                        all_entities[name]["street"] = child.get("v")
            if child.get("k") == "addr:postcode":
                        all_entities[name]["postal"] = child.get("v")
            if child.get("k") == "addr:housenumber":
                        all_entities[name]["house"] = child.get("v")
            if child.get("k") == "addr:city":
                        all_entities[name]["city"] = child.get("v")
            

            if child.get("k") == "phone" or child.get("k") == "contact:phone":
                        all_entities[name]["phone"] = child.get("v")
            if child.get("k") == "opening_hours":
                        all_entities[name]["opening_hours"] = child.get("v")
            if child.get("k") == "name":
                        all_entities[name]["name"] = child.get("v")

# Check number of relevant entities
print("Num available nodes: ", websitesNoFace)
print(str(all_entities))

with open("EntitiesOSMData", "wb") as data:
    pickle.dump(all_entities, data, protocol=2)

websites.close()
