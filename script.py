#!/usr/bin/env python
# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
import re
import pickle
import subprocess
import os
import sys

argument = sys.argv[1]

if argument == "help":
    print("\nCOMMANDS LIST:")
    print("prepare: runs all necessary commands so the system can be trained")
    print("train: runs all necessary commands to train the system")
    print("test <list of websites>: adds entities' websites to be scraped and runs all necessary commands to test the system")
    print("\n")

elif argument == "prepare":
    # Delete irrelevant files
    #os.system('rm websites.txt')
    #os.system('rm GEO_CMS_Data/addresses_cms.txt')

    # Extract .osm file
    #subprocess.call(["bzip2", "-cdk", "../portugal-latest.osm.bz2"], stdout=open("portugal-latest.osm", "w"))

    # Extract entities information 
    #os.system('echo "Extract entities information from OSM data"')
    #subprocess.call(["../osmosis/bin/osmosis", "--read-xml", "../portugal-latest.osm", "--tf", "accept-nodes", "amenity=*", "--tf", "reject-nodes", "amenity=charging_station", "--tf", "reject-relations", "--tf", "reject-ways", "--write-xml", "amenity_v8_04.osm.xml"])
    #os.system('echo "OSM data extracted"')

    # Extract list of websites from the OSM information
    #os.system('echo "Create websites list"')
    #subprocess.call(["python", "convertsXMLIntoWebsites.py"])
    #os.system('echo "The List is Created!"')

    # Extract all addresses provided by the municipalities
    os.system('echo "Gathering municipalities information!"')
    os.chdir("GEO_CMS_Data")
    subprocess.call(["python", "geoCMS.py"])
    os.chdir("../")
    os.system('echo "Finished extraction of municipalities\' address information!"')

    # Extract addresses from the yellow pages
    #os.system('echo "Extract address information from yellow pages"')
    #subprocess.call(["rm", "yellow/pai_log_file.txt"])
    #subprocess.call(["rm", "yellow/paiStreets.txt"])
    #g = open("yellow/paierror.txt", "w")
    #os.chdir("yellow")
    #subprocess.call(["scrapy", "crawl", "yellow"], stdout = g, stderr = subprocess.STDOUT)
    #g.close()
    #subprocess.call(["python", "paiStreetExtractor.py"])
    #os.chdir("../")
    #os.system('echo "Finished collecting all address information..."')

    # Extract HTML pages for all web pages 
    #os.system('echo "Delete scrapy files!"')
    #subprocess.call(["rm", "-r", "script/files"])
    #subprocess.call(["mkdir", "script/files"])
    #subprocess.call(["rm", "script/log_spider.txt"])
    #subprocess.call(["rm", "script/log_parsing.txt"])
    #subprocess.call(["rm", "script/outputerror.txt"])
    #os.system('echo "Files deleted!"')
    #os.system('echo "SCRAPING TIME! BEGIN!"')
    #f = open("script/outputerror.txt", "w")
    #subprocess.call(["scrapy", "crawl", "script"], stdout = f, stderr =  subprocess.STDOUT)
    #f.close()
    #os.system('echo "SPIDER SMASHED!"')


elif argument == "train":

    # Train language models
    os.system('echo "Begin language modelling training"')
    os.chdir("languagemodels")
    subprocess.call(["python", "nltk_language_model.py", "train_lms"])
    os.chdir("../")
    os.system('echo "Finished training for language models"')

    # Train classifiers
    os.system('echo "Begin classification training"')
    os.chdir("classification")
    subprocess.call(["python", "classificationTests.py", "prepare"])
    subprocess.call(["python", "classificationTests.py", "train"])
    os.chdir("../")
    os.system('echo "Finished training for classifiers"')


elif argument == "results":

    # Correct files syntax and apply rules to the files
    os.system('echo "Get rules results"')
    subprocess.call(["python", "parsingLibraries.py", "2"])
    subprocess.call(["python", "parsingLibraries.py", "3"])
    os.system('echo "Candidates welcomed oh oh oh"')

    # Get statistics based on the rules obtained
    os.system('echo "Get rules metrics results"')
    subprocess.call(["python", "parsingLibraries.py", "4"])
    os.system('echo "Candidates welcomed oh oh oh"')

    # Get performance statistics based on the language models
    os.system('echo "Now lets get to language modelling testing"')
    os.chdir("languagemodels")
    subprocess.call(["python", "nltk_language_model.py", "test_lm_kneser"])
    subprocess.call(["python", "nltk_language_model.py", "test_lm_witten"])
    subprocess.call(["python", "nltk_language_model.py", "test_lm_laplace"])
    os.chdir("../")
    os.system('echo "Tired of testing for language modelling"')

    # Get performance statistics based on the classifiers
    os.system('echo "Now lets start to test the classifiers"')
    os.chdir("classification")
    subprocess.call(["python", "classificationTests.py", "test"])
    os.chdir("../")
    os.system('echo "Tired of testing"')


elif argument == "test":

    # Save user input
    if sys.argv[2] is None:
        print("User List not added")
    else:
        user_list = []
        for element in sys.argv[2:]:
            print(str(element))
            user_list.append(element)

        with open("UserWebsites", "wb") as f:
            pickle.dump(user_list, f, protocol=2)


        # Scrape websites given
        os.system('echo "Eliminating old scraping files!"')
        
        if not os.path.isdir("user/user/files"):
            subprocess.call(["mkdir", "user/user/files"])
        else:
            subprocess.call(["rm", "-rf", "user/user/files"])
            subprocess.call(["mkdir", "user/user/files"])
        
        subprocess.call(["rm", "user/user_log_spider.txt"])
        subprocess.call(["rm", "user/user_log_parsing.txt"])
        subprocess.call(["rm", "user/user_outputerror.txt"])
        os.system('echo "User scraping old files deleted!"')

        os.chdir("user")
        os.system('echo "Begin Scraping"')
        f = open("user_outputerror.txt", "w")
        subprocess.call(["scrapy", "crawl", "user"], stdout = f, stderr =  subprocess.STDOUT)
        f.close()
        os.chdir("../")
        os.system('echo "Finished Scraping"')


        # Apply rules to user files
        os.system('echo "Apply rules to file"')
        
        if not os.path.isdir("user/entities"):
            subprocess.call(["mkdir", "user/entities"])
        else:
            subprocess.call(["rm", "-rf", "user/entities"])
            subprocess.call(["mkdir", "user/entities"])

        subprocess.call(["python", "parsingUserLibraries.py", "1"])
        subprocess.call(["python", "parsingUserLibraries.py", "3"])
        os.system('echo "Done with the rules"')


        if not os.path.isdir("results"):
            subprocess.call(["mkdir", "results"])

        # Apply language models to user files
        os.system('echo "Apply language models to file"')
        os.chdir("languagemodels")
        subprocess.call(["python", "nltk_language_model.py", "test_lms"])
        os.chdir("../")
        os.system('echo "Done with the languague models"')


        # Apply language models to user files
        os.system('echo "Apply classifiers to file"')
        os.chdir("classification")
        subprocess.call(["python", "classificationTests.py", "user_test"])
        os.chdir("../")
        os.system('echo "Done with the classifiers"')
