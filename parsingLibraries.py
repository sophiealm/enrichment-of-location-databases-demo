#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import BeautifulSoup
from bs4 import BeautifulSoup
# Import regular expressions
import re
# Import to handle folders and files in them
import os
import sys
import chardet
from bs4 import UnicodeDammit
# Imports from libpostal library to parse addresses
#from postal.expand import expand_address
#from postal.parser import parse_address
# Parsing of dates fom dateparser library DOESNT WORK FOR WEEK DAYS
#import dateparser
# Parsing opening hours with humanized_opening_hours
#import humanized_opening_hours as hoh

def addressStarter(p):
		return re.search(r"\bRUA\b.*[0-9]+|\bR\. .*[0-9]+|\bR\b.*[0-9]+|\bPRAÇA\b.*[0-9]+|\bP\. .*[0-9]+|\bP\b.*[0-9]+|\bPRAÇETA\b.*[0-9]+|\bLARGO\b.*[0-9]+|\bLUGAR\b.*[0-9]+|\bQUINTA\b.*[0-9]+|\bQ\. .*[0-9]+|\bQ\b.*[0-9]+|\bS[ÍI]TIO\b.*[0-9]+|\bAVENIDA\b.*[0-9]+|\bAV\. .*[0-9]+|\bAV\b.*[0-9]+", str(p).upper())

# Runs RE rules and decides if it is an address
def isAddress(previous_p, p):
		return addressStarter(p) or re.search("[0-9]{4}–[0-9]{3} ", str(p).upper()) or (addressStarter(previous_p) and re.search("[0-9]{4}–[0-9]{3} ", str(p).upper())) 

# Runs RE rules and decides if it is an opening schedule
def isOpeningHours(pre_previous_p, previous_p, p, next_p, f):

		if re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper())and not addressStarter(p) and not addressStarter(previous_p):

		 	return re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper()) and not addressStarter(p) and not addressStarter(previous_p)

		elif re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(next_p).upper()) and not addressStarter(p) and not addressStarter(next_p):

		 	return re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(next_p).upper()) and not addressStarter(p) and not addressStarter(next_p)


		elif re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(pre_previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper()) and not addressStarter(p) and not addressStarter(pre_previous_p):

		 	return re.search(r"(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b)", str(pre_previous_p).upper()) and re.search(r"(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)", str(p).upper()) and not addressStarter(p) and not addressStarter(pre_previous_p)

		return re.search(r'(2ª|3ª|4ª|5ª|6ª|\bSEG\b|\bTER\b|\bQUA\b|\bQUI\b|\bSEX\b|\bSÁB\b|\bSAB\b|\bDOM\b|\bS\b|\bT\b|\bQ\b|\bD\b|\bSEGUNDA([ –]{0,1}FEIRA{0,1}|)\b|\bTERÇA([ –]{0,1}FEIRA{0,1}|)\b|\bQUARTA([ –]{0,1}FEIRA{0,1}|)\b|\bQUINTA([ –]{0,1}FEIRA{0,1}|)\b|\bSEXTA([ –]{0,1}FEIRA{0,1}|)\b|\bSÁBADO\b|\bSABADO\b|\bDOMINGO\b|\bDIAS [ÚU]TEIS\b).*(\b[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}[ ]*([–/>]|ÀS){1}[ ]*[0-9]{1,2}H{0,1}[ :]{0,3}[0-9]{0,2}H{0,1}\b)', str(p).upper()) and not addressStarter(p)


def isPhone(p):
		return re.search(r"\b[+]{0,1}[(]{0,1}[0-9]{0,3}[)]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{3}\b", str(p)) \
			or re.search(r"\b[+]{0,1}[(]{0,1}[0-9]{0,3}[)]{0,1}[0-9]{2}[ ]{0,1}[0-9]{3}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}\b", str(p)) \
			or re.search(r"\b[+]{0,1}[(]{0,1}[0-9]{0,3}[)]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{2}[ ]{0,1}[0-9]{1}\b", str(p))
				


# HTML files home folder
filesLocation = "script/files/"
counter = 0

# Entities to annotate
entities = ["sharisushibar", "restaurante-sol-e-mar", "restaurantedoclubenavaldelisboa", "jardimdosentidos", "aquariumcoimbra", \
			"gaviaonovo", "8healthlounge", "restaurantedonpedro1", "sakeportugal", "clinicadentariajardimdosarcos", \
			"hennessys-pub", "doiscorvos", "salpoente", "eduardomerino", "centromedicodetondela", "vetdinha", "dnuno", \
			"restauranteacantarinha", "cufra", "palaciochiado", "restaurantelilys", "restaurantemarradas", "voltaemeia", \
			"INATEL", "farmaciacodeco", "povolisboa", "espacodoluto", "caldeirasevulcoes", "ladobcafe", "ovitral" \
			]

print("Datasets for annotations to be created -> 1\nProccess annotations information? -> 2\nNormalize sentences split in half with bad annotations -> 3\nCheck accuracy annotation score -> 4\n")

letter = sys.argv[1]

if letter == "1":

	entities_files = []
	current_entity = None
	count_2 = 0

	for entity in entities:
		name_entity = "entities_prov/" + entity + ".txt"
		entities_files.append(open(name_entity, "w", encoding="UTF-8"))

	# Goes file by file in numerical order
	while counter != len(os.listdir(filesLocation)):
			counter += 1
			filename = "file" + str(counter) + ".txt"

			# Opens specific file to read
			analyze = open(os.path.join(filesLocation, filename), "rb")

			# Takes out first line that is the URL
			url = analyze.readline().decode("UTF-8")
			encoding = analyze.readline().decode("UTF-8")
			print(encoding)
			#writingAttr.write(str(url) + "\n")

			# Check if file is a selected entity in that case we want to save it
			for entity in entities:
				m = re.search(entity, url.lower())
				if m:
					current_entity = entity
			
			content = analyze.read()#.decode('UTF-8')
			#print(content)

			# result = chardet.detect(content)
			# charenc = result['encoding']
			# print(str(result) + "\n\n")
			# if charenc == None:
			# 	charenc = "utf-8"
			# dammit = UnicodeDammit(content, ["latin-1", "utf-8"])
			# charenc = dammit.original_encoding
			# if charenc == None:
			#  	charenc = "utf-8"
			# print(charenc)
			# content = content.decode(charenc)

			soup = BeautifulSoup(content, "html.parser")

			# Eliminate script and style portions of the HTML as they are irrelevant usually
			# MUST CHECK THIS
			for node in soup.find_all(['script', 'style']):
				node.decompose()

			if current_entity != None:
				#print("FILE IS PREPARED\n")
				entity_file = entities_files[entities.index(current_entity, 0, len(entities))]
				entity_file.write("\n" + filename + "\n")
				count_2 += 1

			print(current_entity)
			# Follows all strings and analyzes it
			for p in soup.stripped_strings:
				try:
					if current_entity != None:
						#print("File is being writen\n")
						entity_file.write(p + " 0 \n")
				except Exception as e:
					print(e)

			current_entity = None

			# Close current HTML file and go to next
			analyze.close()

	for entity in entities_files:
		entity.close()

	print(counter)
	print(count_2)

# Close answer files
#writingAttr.close()

elif letter == "2":

	bad_phrase = 0
	prov_phrase = ""

	for entity in entities:
		name_entity_rules = "annotated/" + entity + ".txt"
		entityFileRules = open(name_entity_rules, "r")

		name_entity_rules1 = "annotated/rules_" + entity + ".txt"
		entityFileRules1 = open(name_entity_rules1, "w")

		for line in entityFileRules:

			if line == "" or line == "\n":
				continue

			if re.search(r'\bfile', line):
				entityFileRules1.write(str(line))
				continue

			line_split = line.split()
			attribute_type = line_split[-1].isdigit()
			#print(attribute_type)

			if attribute_type == True and bad_phrase == 0:
				entityFileRules1.write(str(line))

			elif attribute_type == True and bad_phrase == 1:
				entityFileRules1.write(str(prov_phrase))
				bad_phrase = 0
				prov_phrase = ""

			else:
				prov_phrase = prov_phrase + line.rstrip('\n') + " "
				
				bad_phrase = 1

		entityFileRules.close()
		entityFileRules1.close()

elif letter == "3":

	# Apply rules to annotated files
	for entity in entities:
		name_entity = "annotated/rules_" + entity + ".txt"
		entityFile = open(name_entity, "r")

		name_entity_rules = "annotated/rules1_" + entity + ".txt"
		entityFileRules = open(name_entity_rules, "w")

		previous_line = ""
		pre_previous_line = ""
		flags = {0: 0, 1: 0, 2: 0}

		lines = entityFile.readlines()
		for ind in range(0, len(lines)-1):
			lines[ind] = re.sub("-", "–", lines[ind])
			line_split = lines[ind].split()
			p =  " ".join(line_split[0:-1])

			previous_line_split = previous_line.split()
			previous_p =  " ".join(previous_line_split[0:-1])

			pre_previous_line_split = pre_previous_line.split()
			pre_previous_p =  " ".join(pre_previous_line_split[0:-1])

			next_line = re.sub("-", "–", lines[ind+1])
			next_line_split = next_line.split()
			next_p =  " ".join(next_line_split[0:-1])

			# Morada com RE
			if isAddress(previous_p, p):
				#p = parse_address(p)
				entityFileRules.write("ADDRESS1: " + str(lines[ind]))
				flags[0] += 1

			# Horario com RE
			if isOpeningHours(pre_previous_p, previous_p, p, next_p, entityFileRules):
				entityFileRules.write("HOURS2: " + str(lines[ind]))
				flags[1] += 1
				#print(p)
				#print(normalizeDate(p))
				#oh = hoh.OHParser(normalizeDate(p))
				#writingHours.write("Horas" + '\n'.join(oh.render().full_description()) + "\n")

			# Horario com RE
			if isPhone(p):
				entityFileRules.write("PHONE3: " + str(lines[ind]))
				flags[2] += 1

			if not isAddress(previous_p, p) and not isOpeningHours(pre_previous_p, previous_p, p, next_p, entityFileRules) and not isPhone(p):
				entityFileRules.write(str(lines[ind]))
		
			pre_previous_line = previous_line
			previous_line = lines[ind]

		print("Flags: " + str(flags[0]) + " " + str(flags[1]) + " " + str(flags[2]))
		if flags[2] == 0:
			print(str(entity) + " PHONE: NOT FOUND")

		if flags[1] == 0:
			print(str(entity) + " OPENING HOURS: NOT FOUND")

		if flags[0] == 0:
			print(str(entity) + " ADDRESS: NOT FOUND")

		entityFile.close()
		entityFileRules.close()



elif letter == "4":

	print("Metric rules scores por entidade: ")
	accuracies = {"Morada": [], "Horas": [], "Telefone": []}
	precisions = {"Morada": [], "Horas": [], "Telefone": []}
	recalls = {"Morada": [], "Horas": [], "Telefone": []}
	f_scores = {"Morada": [], "Horas": [], "Telefone": []}
	precisions_top = {"Morada": [], "Horas": [], "Telefone": []}
	precisions_bottom = {"Morada": [], "Horas": [], "Telefone": []}
	recall_top = {"Morada": [], "Horas": [], "Telefone": []}
	recall_bottom = {"Morada": [], "Horas": [], "Telefone": []}


	with open("results/REResults_Rules.txt", "w", encoding="utf-8") as g:
		for entity in entities:
			g.write("\n\nEntity: " + entity + "\n")
			name_entity_rules = "annotated/rules1_" + entity + ".txt"
			entityFileRules = open(name_entity_rules, "r", encoding="utf-8")

			eval_dic = {"Morada 1 Sistema 1": 0, "Morada 0 Sistema 1": 0, "Morada 1 Sistema 0": 0, "Morada 0 Sistema 0": 0, \
						"Horas 1 Sistema 1": 0, "Horas 0 Sistema 1": 0, "Horas 1 Sistema 0": 0, "Horas 0 Sistema 0": 0, \
						"Telefone 1 Sistema 1": 0, "Telefone 0 Sistema 1": 0, "Telefone 1 Sistema 0": 0, "Telefone 0 Sistema 0": 0}
			score = 0

			for line in entityFileRules:

				if line == "" or line == "\n":
					continue

				if re.search(r'\bfile', line):
					continue

				line_split = line.split()
				attribute_type = int(line_split[-1])
				line = " ".join(line_split[0:-1])

				if re.search("ADDRESS1: ",  line):
					if attribute_type == 1:
						eval_dic["Morada 1 Sistema 1"] += 1
					else:
						eval_dic["Morada 0 Sistema 1"] += 1

				elif attribute_type == 1 and not re.search("PHONE3: ",  line) and not re.search("HOURS2: ",  line):
					eval_dic["Morada 1 Sistema 0"] += 1
				elif re.search("PHONE3: ",  line) or re.search("HOURS2: ",  line):
					print("Bad example for address")
				else:
					eval_dic["Morada 0 Sistema 0"] += 1

				if re.search("HOURS2: ",  line):
					if attribute_type == 2:
						eval_dic["Horas 1 Sistema 1"] += 1
					else:
						eval_dic["Horas 0 Sistema 1"] += 1

				elif attribute_type == 2 and not re.search("ADDRESS1: ",  line) and not re.search("PHONE3: ",  line):
					eval_dic["Horas 1 Sistema 0"] += 1
				elif re.search("PHONE3: ",  line) or re.search("ADDRESS1: ",  line):
					print("Bad example for hours")
				else:
					eval_dic["Horas 0 Sistema 0"] += 1

				# Telefone -> se e telefone ou nao -> anotacao
				# Sistema -> se o sistema acha que e telefone ou nao
				if re.search("PHONE3: ",  line):
					if attribute_type == 3:
						eval_dic["Telefone 1 Sistema 1"] += 1
					else:
						eval_dic["Telefone 0 Sistema 1"] += 1

				elif attribute_type == 3 and not re.search("ADDRESS1: ",  line) and not re.search("HOURS2: ",  line):
					eval_dic["Telefone 1 Sistema 0"] += 1
				elif re.search("ADDRESS1: ",  line) or re.search("HOURS2: ",  line):
					print("Bad example for phone number")
				else:
					eval_dic["Telefone 0 Sistema 0"] += 1

			
			
			score_m = eval_dic["Morada 1 Sistema 1"] + eval_dic["Morada 0 Sistema 0"]
			score_m /= (eval_dic["Morada 1 Sistema 1"] + eval_dic["Morada 1 Sistema 0"] + eval_dic["Morada 0 Sistema 1"] + eval_dic["Morada 0 Sistema 0"])
			g.write(str(score_m) + "\n")
			accuracies["Morada"].append(score_m)

			score_m_p = eval_dic["Morada 1 Sistema 1"]
			num = (eval_dic["Morada 1 Sistema 1"] + eval_dic["Morada 0 Sistema 1"])
			if num != 0:
				score_m_p /= num
			else:
				score_m_p = 0
			g.write(str(score_m_p) + "\n")
			precisions["Morada"].append(score_m_p)
			precisions_top["Morada"].append(eval_dic["Morada 1 Sistema 1"])
			precisions_bottom["Morada"].append(eval_dic["Morada 1 Sistema 1"] + eval_dic["Morada 0 Sistema 1"])


			score_m_r = eval_dic["Morada 1 Sistema 1"]
			num = (eval_dic["Morada 1 Sistema 1"] + eval_dic["Morada 1 Sistema 0"])
			if num != 0:
				score_m_r /= num
			else:
				score_m_r = 0
			g.write(str(score_m_r) + "\n")
			recalls["Morada"].append(score_m_r)
			recall_top["Morada"].append(eval_dic["Morada 1 Sistema 1"])
			recall_bottom["Morada"].append(eval_dic["Morada 1 Sistema 1"] + eval_dic["Morada 1 Sistema 0"])

			score_m_f = 2 * score_m_p * score_m_r
			num = score_m_p + score_m_r
			if num != 0:
				score_m_f /= num
			else:
				score_m_f = 0
			g.write(str(score_m_f) + "\n")
			f_scores["Morada"].append(score_m_f)


			#g.write("TP: " + str(eval_dic["Horas 1 Sistema 1"]) + ", TN: " + str(eval_dic["Horas 0 Sistema 0"]) + ", FP: " + str(eval_dic["Horas 0 Sistema 1"]) + ", FN: " + str(eval_dic["Horas 1 Sistema 0"]) + "\n\n")
			score_h = eval_dic["Horas 1 Sistema 1"] + eval_dic["Horas 0 Sistema 0"]
			score_h /= (eval_dic["Horas 1 Sistema 1"] + eval_dic["Horas 1 Sistema 0"] + eval_dic["Horas 0 Sistema 1"] + eval_dic["Horas 0 Sistema 0"])
			g.write(str(score_h) + "\n")
			accuracies["Horas"].append(score_h)

			score_h_p = eval_dic["Horas 1 Sistema 1"]
			num = eval_dic["Horas 1 Sistema 1"] + eval_dic["Horas 0 Sistema 1"]
			if num != 0:
				score_h_p /= num
			else:
				score_h_p = 0
			g.write(str(score_h_p) + "\n")
			precisions["Horas"].append(score_h_p)
			precisions_top["Horas"].append(eval_dic["Horas 1 Sistema 1"])
			precisions_bottom["Horas"].append(eval_dic["Horas 1 Sistema 1"] + eval_dic["Horas 0 Sistema 1"])

			score_h_r = eval_dic["Horas 1 Sistema 1"]
			num = eval_dic["Horas 1 Sistema 1"] + eval_dic["Horas 1 Sistema 0"]
			if num != 0:
				score_h_r /= num
			else:
				score_h_r = 0
			g.write(str(score_h_r) + "\n")
			recalls["Horas"].append(score_h_r)
			recall_top["Horas"].append(eval_dic["Horas 1 Sistema 1"])
			recall_bottom["Horas"].append(eval_dic["Horas 1 Sistema 1"] + eval_dic["Horas 1 Sistema 0"])

			score_h_f = 2 * score_h_p * score_h_r
			num = score_h_p + score_h_r
			if num != 0:
				score_h_f /= num
			else:
				score_h_f = 0
			g.write(str(score_h_f) + "\n")
			f_scores["Horas"].append(score_h_f)



			score_t = eval_dic["Telefone 1 Sistema 1"] + eval_dic["Telefone 0 Sistema 0"]
			score_t /= (eval_dic["Telefone 1 Sistema 1"] + eval_dic["Telefone 1 Sistema 0"] + eval_dic["Telefone 0 Sistema 1"] + eval_dic["Telefone 0 Sistema 0"])
			g.write(str(score_m) + "\n")
			accuracies["Morada"].append(score_t)

			score_t_p = eval_dic["Telefone 1 Sistema 1"]
			num = eval_dic["Telefone 1 Sistema 1"] + eval_dic["Telefone 0 Sistema 1"]
			if num != 0:
				score_t_p /= num
			else:
				score_t_p = 0
			g.write(str(score_t_p) + "\n")
			precisions["Telefone"].append(score_t_p)
			precisions_top["Telefone"].append(eval_dic["Telefone 1 Sistema 1"])
			precisions_bottom["Telefone"].append(eval_dic["Telefone 1 Sistema 1"] + eval_dic["Telefone 0 Sistema 1"])

			score_t_r = eval_dic["Telefone 1 Sistema 1"]
			num = eval_dic["Telefone 1 Sistema 1"] + eval_dic["Telefone 1 Sistema 0"]
			if num != 0:
				score_t_r /= num
			else:
				score_t_r = 0
			g.write(str(score_t_r) + "\n")
			recalls["Telefone"].append(score_t_r)
			recall_top["Telefone"].append(eval_dic["Telefone 1 Sistema 1"])
			recall_bottom["Telefone"].append(eval_dic["Telefone 1 Sistema 1"] + eval_dic["Telefone 1 Sistema 0"])

			score_t_f = 2 * score_t_p * score_t_r
			num = score_t_p + score_t_r
			if num != 0:
				score_t_f /= num
			else:
				score_t_f = 0
			g.write(str(score_t_f) + "\n")
			f_scores["Telefone"].append(score_t_f)

			entityFileRules.close()

		g.write("Macro Average Precision Morada: " + str(sum(precisions["Morada"])/len(precisions["Morada"])) + "\n")
		g.write("Micro Average Precision Morada: " + str(sum(precisions_top["Morada"])/sum(precisions_bottom["Morada"])) + "\n")
		g.write("Macro Average Recall Morada: " + str(sum(recalls["Morada"])/len(recalls["Morada"])) + "\n")
		g.write("Micro Average Recall Morada: " + str(sum(recall_top["Morada"])/sum(recall_bottom["Morada"])) + "\n")


		g.write("Macro Average Precision Horas: " + str(sum(precisions["Horas"])/len(precisions["Horas"])) + "\n")
		g.write("Micro Average Precision Horas: " + str(sum(precisions_top["Horas"])/sum(precisions_bottom["Horas"])) + "\n")
		g.write("Macro Average Recall Horas: " + str(sum(recalls["Horas"])/len(recalls["Horas"])) + "\n")
		g.write("Micro Average Recall Horas: " + str(sum(recall_top["Horas"])/sum(recall_bottom["Horas"])) + "\n")

		g.write("Macro Average Precision Telefone: " + str(sum(precisions["Telefone"])/len(precisions["Telefone"])) + "\n")
		g.write("Micro Average Precision Telefone: " + str(sum(precisions_top["Telefone"])/sum(precisions_bottom["Telefone"])) + "\n")
		g.write("Macro Average Recall Telefone: " + str(sum(recalls["Telefone"])/len(recalls["Telefone"])) + "\n")
		g.write("Micro Average Recall Telefone: " + str(sum(recall_top["Telefone"])/sum(recall_bottom["Telefone"])) + "\n")


		
