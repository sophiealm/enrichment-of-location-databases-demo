#!/usr/bin/env python
# -*- coding: utf-8 -*-
import scrapy
import re
from urllib.parse import urlparse
import pickle
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError
from twisted.internet.error import TimeoutError, TCPTimedOutError


class UserSpider(scrapy.Spider):
		name = "user"
		download_delay = 5.0
		# Handle HTTP Redirects
		#handle_httpstatus_list = [302]


		# Keeps track of number of files
		count = 0
		# Keeps track of all urls passed
		urls = []
		# Keeps track of any url and its level
		# Dictionary of {level : list of urls in this level}
		# Will fail when URL doesn't match but Pavel might help with this
		# Fix rest of code
		urls_dict = {1: [], 2: [], 3: [], 4: [], 5: []}
		# Max of links per domain
		domains = {}

		def start_requests(self):
				# Fetches all websites in amenity found by osmosis in OSM
				with open("../UserWebsites", "rb") as f:
					websites = pickle.load(f)

				# Create log file to check files are correctly saved
				with open('user_log_spider.txt', 'w') as log:
					for url in websites:
										
						url = re.sub("/$", "", url)

						# If link registered in OSM is a bad file like png/facebook/youtube/pdf/etc then dont look at it
						if self.bad_file(url):
							log.write("SKIPPED: " + url)
							continue

						# Make sure that all websites have http format so that Scrapy doesnt give a missing schema error
						if not re.search('http', url) and not re.search('https', url) and not re.search('www', url):
							url = 'http://' + url
						if re.search('www', url) and not re.search('http', url):
							url = 'http://' + url
						#elif re.search('http://', url):
						#	url = 'http://' + url

						url_domain = urlparse(url)
						if url_domain.netloc not in self.domains:
								self.domains[url_domain.netloc] = 1

						log.write(url)
						# Makes sure URL doesnt include new line char
						url = url.rstrip('\n')
						# Save "mother URL" in self.urls, a list of all URLS
						self.urls.append(url)
						log.write(str(len(self.urls)) + "\n")

					# Save "mother URLs" in dictionary and assign them level 1
					self.urls_dict[1] = self.urls
			   
				# For all URLS in urls call scrapy parser to fetch their HTMLs and children links
				for url in self.urls:
						yield scrapy.Request(url=url, encoding='utf-8', dont_filter=True, callback=self.parse, errback=self.errback_httpbin)


		# Auxiliary function that returns the depth level of an URL
		def check_level(self, url):
				if url in self.urls_dict.get(5):
						return 5
				elif url in self.urls_dict.get(4):
						return 4
				elif url in self.urls_dict.get(3):
						return 3
				elif url in self.urls_dict.get(2):
						return 2
				elif url in self.urls_dict.get(1):
						return 1
				else:
						# Function returns 7 in case the URL is not in the dictionary (has changed then!)
						return 7

		def keyword_interesting(self, page):
			# Check if it has keyword and is not a picture or pdf
			return re.search('contato', page.lower())  or re.search('contacto', page.lower()) \
					or re.search('contacte', page.lower()) or re.search('contactos', page.lower()) \
					or re.search('informacao', page.lower()) or re.search('informacoes', page.lower()) \
					or re.search('information', page.lower()) or re.search('contate', page.lower()) \
					or re.search('info', page.lower()) or re.search('sobre', page.lower()) \
					or re.search('apresentacao', page.lower()) or re.search('atendimento', page.lower()) \
					or re.search('about', page.lower()) or re.search('quem[. ]{0,1}somos', page.lower()) \
					or re.search('horario', page.lower()) or re.search('home', page.lower()) \
					or re.search('index', page.lower()) or re.search('contact', page.lower()) \
					or re.search(r'\bpt', page.lower()) 

		# Function that returns True if file is a bad file
		def bad_file(self, page):
			return re.search('.jpg', page.lower()) or re.search('.pdf', page.lower()) \
					or re.search('mailto:', page.lower()) or re.search('tel:', page.lower()) \
					or re.search('maps', page.lower()) or re.search('youtube', page.lower()) \
					or re.search('javascript', page.lower()) or re.search('.png', page.lower()) \
					or re.search('facebook', page.lower()) or re.search('.gif', page.lower())


		def already_in_dict(self, page):
			return page in self.urls_dict.get(1) or page in self.urls_dict.get(2) or page in self.urls_dict.get(3) \
					or page in self.urls_dict.get(4) or page in self.urls_dict.get(5)


		def errback_httpbin(self, failure):
			# log all failures
			self.logger.error(repr(failure))

			# in case you want to do something special for some errors,
			# you may need the failure's type:

			if failure.check(HttpError):
				# these exceptions come from HttpError spider middleware
				# you can get the non-200 response
				response = failure.value.response
				self.logger.error('HttpError on %s', response.url)

			elif failure.check(DNSLookupError):
				# this is the original request
				request = failure.request
				self.logger.error('DNSLookupError on %s', request.url)

			elif failure.check(TimeoutError, TCPTimedOutError):
				request = failure.request
				self.logger.error('TimeoutError on %s', request.url)


		def parse(self, response):
			# Create a log file to check errors in parsing function of scrapy
			with open('user_log_parsing.txt', 'a') as log:
				try:
					# Save all relevant response details
					log.write("Status: " + str(response.status) + "\n")
					response_url = re.sub("/$", "", response.url)
					log.write("\nCURRENT_URL: " + response_url + "\n")
					if 'redirect_urls' in response.request.meta:
						log.write("Redirects: " + str(response.request.meta['redirect_urls']) + "\n\n")
					log.write("Response.Headers: " + str(response.headers) + "\n")

					# Create name for each HTML file based on class count attribute
					self.count += 1
					# Define filename based on self.count
					filename = "user/files/file" + str(self.count) + ".txt"

					# Open file and save the html of the webpage contained in the response object
					log.write('Try file ' + filename + "\n")
					with open(filename, 'wb') as f:
						f.write(response.url.encode('utf-8'))
						f.write("\n".encode('utf-8'))
						f.write(response.body.decode('utf-8').encode('utf-8'))
					log.write('Saved file ' + filename + "\n")


					log.write(str(self.urls_dict) + "\n")
					log.write("\nCURRENT_URL: " + response_url + "\n")

					# Check depth level of the current URL
					current_level = self.check_level(response_url)
					log.write("CURRENT_LEVEL: " + str(current_level) + "\n")

					if current_level == 7:
						# response.request.meta['redirect_urls'][0] is always in dictionary
						if 'redirect_urls' in response.request.meta:
							log.write("Redirects level 7: " + str(response.request.meta['redirect_urls']) + "\n")
							log.write("Original url level 7: " + response_url + "\n")
							redirect = response.request.meta['redirect_urls'][0]
							redirect = re.sub("/$", "", redirect)
							current_level = self.check_level(redirect)
							log.write("LEVEL OF EARLY REDIRECT: " + str(current_level) + "\n")
						else:
							log.write("7BUTNOREDIRECT\n")
							current_level = self.check_level(response_url)
							log.write("LEVEL OF EARLY REDIRECT: " + str(current_level) + "\n")

					if current_level == 7:
						log.write("SEVEN\n")

					# If level is five or more we want to stop checking sub-levels
					if current_level < 4:
							# Capture all links in HTML <a> tags in order to follow possible relevant links
							next_page = response.xpath('//a/@href').getall()
							log.write("yooo\n")
							log.write("Next pages: " + str(next_page) + "\n")

							# Follow all children pages of current URL
							for page in next_page:

									page = re.sub("/$", "", page)

									log.write("PAGE: " + page + "\n")

									# If page is pdf/etc ignore it
									if self.bad_file(page):
										log.write("BAD FILE TYPE\n")
										continue

									# urlparse recognizes a netloc only if it is properly introduced by //
									if re.search('www', page) and not re.search('http', page):
										page = 'http://' + page

									# Check if domain of subpage and top page is the same
									# If subpag is not from the same domain we ignore
									split_url = urlparse(response_url)
									domain = split_url.netloc

									split_url_sub = urlparse(page)
									domain_sub = split_url_sub.netloc

									log.write("Domain: " + domain + "\n")
									log.write("Domain SUB: " + domain_sub + "\n")

									if domain_sub != "" and domain_sub != domain:
										log.write("\nDIFFERENT DOMAINS!\n\n")
										continue

									log.write("Domain is empty? " + domain_sub + "\n")
									if domain_sub in self.domains:
										log.write("Domain already visited!\n")
										# Uncomment if a number of same domain pages visited limit is desired
										#if self.domains.get(domain_sub) > 10:
										#	log.write("Too many pages of same domain!\n")
										#	continue

									# Check if page is already in dictionary, if it is ignore it
									if self.already_in_dict(page):
										log.write("Page already in dictionary!!\n")
										continue

									log.write("PAGE NOT IN DICT: " + page + "\n")

									url = split_url_sub.path
									if split_url_sub.fragment != "":
											url = url + split_url_sub.fragment
									log.write("Path: " + url + "\n")

									# Check if sublink doesnt have a relevant keyword and in that case skip it
									if current_level != 1 and not self.keyword_interesting(url):
										log.write("Skipped page: " + url + "\n")
										continue

									log.write("PAGE SAME DOMAIN AND INTERESTING: " + page + "\n")

									# If link is in level of depth (1) or (2 or more with relevant keywords) then download it all
									# If page is not a full link then join it to previous url, otherwise parse new full link
									if re.search('http://', page) or re.search('https://', page):

											self.log('Previous page was: ' + response.url + '/ Next full page is: ' + page + "\n")

											# Add new sublink to dictionary with deeper depth level
											# Add links only if they have interesting keywords in them
											self.urls_dict[current_level + 1].append(page)
											log.write("PAGE_dict: " + page + "\n")

											split_url = urlparse(page)
											url = split_url.netloc
											if url in self.domains:
													self.domains[url] +=1
											else:
													self.domains[url] = 1

											log.write("Next Scrapy page: " + str(page) + "\n")
											yield scrapy.Request(page, encoding='utf-8', dont_filter=True, callback=self.parse, errback=self.errback_httpbin)

									elif re.search('//www.', page) or re.search('www.', page):

											if re.search('//www.', page):
													page = 'http:' + page
											elif re.search('www.', page):
													page = 'http://' + page

											log.write('Previous page was: ' + response.url + '/ Next full www page is: ' + page + "\n")

											# Add new sublink to dictionary with deeper depth level
											# Add links only if they have interesting keywords in them
											self.urls_dict[current_level + 1].append(page)
											log.write("PAGE_dict: " + page + "\n")

											split_url = urlparse(page)
											url = split_url.netloc
											if url in self.domains:
													self.domains[url] +=1
											else:
													self.domains[url] = 1

											log.write("Next Scrapy page: " + str(page) + "\n")
											yield scrapy.Request(page, encoding='utf-8', dont_filter=True, callback=self.parse, errback=self.errback_httpbin)

									else:
											log.write('Previous page was: ' + response.url + '/ Next sub page is: ' + page + '\n')
											self.log("\n\n              NEW PAGE                 \n\n")

											base_page = response.urljoin(page)

											# Check if page is already in dictionary, if it is ignore it
											if self.already_in_dict(base_page):
												log.write("Page with base already in dictionary!!\n")
												continue

											log.write("PAGE WITH BASE NOT IN DICT: " + page + "\n")

											split_url = urlparse(page)
											url = split_url.path
											if split_url.fragment != "":
													url = url + split_url.fragment
											log.write("Path: " + url + "\n")

											# Check if sublink doesnt have a relevant keyword and in that case skip it
											if not self.keyword_interesting(url):
												log.write("Skipped page: " + url + "\n")
												continue

											url_domain = split_url.netloc
											if url_domain in self.domains:
												self.domains[url_domain] +=1
											else:
												self.domains[url_domain] = 1
											

											log.write("PAGE WITH BASE SAME DOMAIN: " + page + "\n")

											# Add new sublink to dictionary with deeper depth level
											# Add links even only if they have interesting keywords in them
											self.urls_dict[current_level + 1].append(base_page)
											log.write("PAGE_dict: " + base_page + "\n")

											log.write("Next Scrapy page: " + str(base_page) + "\n")
											yield scrapy.Request(base_page, encoding='utf-8', dont_filter=True, callback=self.parse, errback=self.errback_httpbin)



				except Exception as e:
						log.write("\nEXCEPTION:\n")
						log.write(str(response.url) + "\n")
						log.write("e: " + str(e))
						log.write("\n")
