https://www.udon.com/pt/en/people/join-the-team/
<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
	<meta charset="UTF-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link rel="pingback" href="https://www.udon.com/pt/xmlrpc.php" />

		<link rel="shortcut icon" href="//www.udon.com/pt/wp-content/uploads/sites/9/2019/07/favicon.png" type="image/x-icon" />
		<link rel="apple-touch-icon" href="//www.udon.com/pt/wp-content/uploads/sites/9/2019/07/favicon.png">
		<link rel="apple-touch-icon" sizes="120x120" href="//www.udon.com/pt/wp-content/uploads/sites/9/2019/07/favicon.png">
		<link rel="apple-touch-icon" sizes="76x76" href="//www.udon.com/pt/wp-content/uploads/sites/9/2019/07/favicon.png">
		<link rel="apple-touch-icon" sizes="152x152" href="//www.udon.com/pt/wp-content/uploads/sites/9/2019/07/favicon.png">

<title>Join the team - pt</title>
<link rel="alternate" hreflang="pt" href="https://www.udon.com/pt/pt-pt/pessoas/una-se-a-nossa-equipa/" />
<link rel="alternate" hreflang="es" href="https://www.udon.com/pt/personas/unete-al-equipo/" />
<link rel="alternate" hreflang="en" href="https://www.udon.com/pt/en/people/join-the-team/" />

<!-- This site is optimized with the Yoast SEO Premium plugin v7.4.2 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://www.udon.com/pt/en/people/join-the-team/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Join the team - pt" />
<meta property="og:url" content="https://www.udon.com/pt/en/people/join-the-team/" />
<meta property="og:site_name" content="pt" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="Join the team - pt" />
<!-- / Yoast SEO Premium plugin. -->

<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="pt &raquo; Feed" href="https://www.udon.com/pt/en/feed/" />
<link rel="alternate" type="application/rss+xml" title="pt &raquo; Comments Feed" href="https://www.udon.com/pt/en/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/www.udon.com\/pt\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.6"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
	<link rel='stylesheet' id='wp-block-library-css'  href='https://www.udon.com/pt/wp-includes/css/dist/block-library/style.min.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-theme-css'  href='https://www.udon.com/pt/wp-includes/css/dist/block-library/theme.min.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://www.udon.com/pt/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='https://www.udon.com/pt/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.8' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
.tparrows:before{color:#0088cc;text-shadow:0 0 3px #fff;}.revslider-initialised .tp-loader{z-index:18;}
</style>
<link rel='stylesheet' id='tf-compiled-options-mobmenu-css'  href='https://www.udon.com/pt/wp-content/uploads/sites/9/dynamic-mobmenu.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='tf-google-webfont-dosis-css'  href='//fonts.googleapis.com/css?family=Dosis%3Ainherit%2C400&#038;subset=latin%2Clatin-ext&#038;ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='js_composer_front-css'  href='https://www.udon.com/pt/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.7' type='text/css' media='all' />
<link rel='stylesheet' id='bsf-Defaults-css'  href='https://www.udon.com/pt/wp-content/uploads/sites/9/smile_fonts/Defaults/Defaults.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='ultimate-style-min-css'  href='https://www.udon.com/pt/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/ultimate.min.css?ver=3.16.26' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='https://www.udon.com/pt/wp-content/uploads/sites/9/porto_styles/bootstrap.css?ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='porto-plugins-css'  href='https://www.udon.com/pt/wp-content/themes/porto/css/plugins.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='porto-theme-css'  href='https://www.udon.com/pt/wp-content/themes/porto/css/theme.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='porto-shortcodes-css'  href='https://www.udon.com/pt/wp-content/uploads/sites/9/porto_styles/shortcodes.css?ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='porto-dynamic-style-css'  href='https://www.udon.com/pt/wp-content/uploads/sites/9/porto_styles/dynamic_style.css?ver=4.9.7' type='text/css' media='all' />
<link rel='stylesheet' id='porto-style-css'  href='https://www.udon.com/pt/wp-content/themes/porto/style.css?ver=5.2.6' type='text/css' media='all' />
<style id='porto-style-inline-css' type='text/css'>
	#header .logo,.side-header-narrow-bar-logo{max-width:120px;}@media (min-width:1200px){#header .logo{max-width:120px;}}@media (max-width:991px){#header .logo{max-width:110px;}}@media (max-width:767px){#header .logo{max-width:110px;}}#header.sticky-header .logo{width:100px;}#header,.sticky-header .header-main.sticky{border-top:5px solid #ededed}@media (min-width:992px){#header{margin:-35px 0px -30px 0px;}}.page-top .page-title-wrap{line-height:0;}.page-top .page-title:not(.b-none):after{content:'';position:absolute;width:100%;left:0;border-bottom:5px solid #0088cc;bottom:-17px;}#header .header-main #main-menu{display:initial;}#header .header-contact{border-right:none;}#header:not(.sticky-header) .header-main .mega-menu:after{border-right:0px solid #000000;}
</style>
<!--[if lt IE 10]>
<link rel='stylesheet' id='porto-ie-css'  href='https://www.udon.com/pt/wp-content/themes/porto/css/ie.css?ver=5.2.6' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='styles-child-css'  href='https://www.udon.com/pt/wp-content/themes/porto-child/style.css?ver=5.2.6' type='text/css' media='all' />
<link rel='stylesheet' id='custom_style-css'  href='https://www.udon.com/pt/wp-content/themes/porto-child/custom_style.css?ver=5.2.6' type='text/css' media='all' />
<script type='text/javascript' src='https://www.udon.com/pt/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.8'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpml_browser_redirect_params = {"pageLanguage":"en","languageUrls":{"pt_PT":"https:\/\/www.udon.com\/pt\/pt-pt\/pessoas\/una-se-a-nossa-equipa\/","pt":"https:\/\/www.udon.com\/pt\/pt-pt\/pessoas\/una-se-a-nossa-equipa\/","PT":"https:\/\/www.udon.com\/pt\/pt-pt\/pessoas\/una-se-a-nossa-equipa\/","pt-pt":"https:\/\/www.udon.com\/pt\/pt-pt\/pessoas\/una-se-a-nossa-equipa\/","es_ES":"https:\/\/www.udon.com\/pt\/personas\/unete-al-equipo\/","es":"https:\/\/www.udon.com\/pt\/personas\/unete-al-equipo\/","ES":"https:\/\/www.udon.com\/pt\/personas\/unete-al-equipo\/","en_US":"https:\/\/www.udon.com\/pt\/en\/people\/join-the-team\/","en":"https:\/\/www.udon.com\/pt\/en\/people\/join-the-team\/","US":"https:\/\/www.udon.com\/pt\/en\/people\/join-the-team\/"},"cookie":{"name":"_icl_visitor_lang_js","domain":"www.udon.com","path":"\/","expiration":24}};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/sitepress-multilingual-cms/dist/js/browser-redirect/app.js?ver=4.2.8'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate.min.js?ver=3.16.26'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate_bg.min.js?ver=5.2.6'></script>
<link rel='https://api.w.org/' href='https://www.udon.com/pt/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://www.udon.com/pt/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://www.udon.com/pt/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.2.6" />
<link rel='shortlink' href='https://www.udon.com/pt/en/?p=781' />
<link rel="alternate" type="application/json+oembed" href="https://www.udon.com/pt/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.udon.com%2Fpt%2Fen%2Fpeople%2Fjoin-the-team%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://www.udon.com/pt/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fwww.udon.com%2Fpt%2Fen%2Fpeople%2Fjoin-the-team%2F&#038;format=xml" />

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			ga('create', 'UA-33143244-2', 'auto');
			
			ga('send', 'pageview');
		</script>

	<meta name="generator" content="WPML ver:4.2.8 stt:1,41,2;" />
		<script type="text/javascript">
		WebFontConfig = {
			google: { families: [ 'Arial%2C+Helvetica%2C+sans-serif:200,300,400,700,800','Shadows+Into+Light:200,300,400,700,800','Playfair+Display:200,300,400,700,800','Open+Sans:200,300,400,700,800' ] }
		};
		(function(d) {
			var wf = d.createElement('script'), s = d.scripts[0];
			wf.src = 'https://www.udon.com/pt/wp-content/themes/porto/js/libs/webfont.js';
			wf.async = true;
			s.parentNode.insertBefore(wf, s);
		})(document);</script>
		
<!-- Facebook Pixel Code -->
<script type='text/javascript'>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
</script>
<!-- End Facebook Pixel Code -->
<script type='text/javascript'>
  fbq('init', '202983730158820', [], {
    "agent": "wordpress-5.2.6-1.7.25"
});
</script><script type='text/javascript'>
  fbq('track', 'PageView', []);
</script>
<!-- Facebook Pixel Code -->
<noscript>
<img height="1" width="1" style="display:none" alt="fbpx"
src="https://www.facebook.com/tr?id=202983730158820&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="https://www.udon.com/pt/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.4.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">function setREVStartSize(e){									
						try{ e.c=jQuery(e.c);var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
							if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})					
						}catch(d){console.log("Failure at Presize of Slider:"+d)}						
					};</script>
<style type="text/css" title="dynamic-css" class="options-output">.alternative-font, a, .color-primary{color:#0088cc;}#footer,#footer p{line-height:28px;font-size:13px;}.header-wrapper{background-color:#000000;}#header .header-main{background-color:#000000;}#header, #header .header-main .header-contact .nav-top > li > a, #header .top-links > li.menu-item:before{color:#999999;}#header .header-top, .header-top .top-links>li.menu-item:after{color:#777777;}.page-top{background-color:#171717;}#footer{background-color:#ffffff;}#footer .footer-bottom{background-color:#ffffff;}#footer .footer-bottom, #footer .footer-bottom p, #footer .footer-bottom .widget > div > ul li, #footer .footer-bottom .widget > ul li{color:#2d2d2d;}#header.sticky-header .searchform-popup .search-toggle{color:#777777;}#header.sticky-header .searchform-popup .search-toggle:hover{color:#0088cc;}#mini-cart .cart-subtotal, #mini-cart .minicart-icon{color:#0088cc;}#mini-cart .cart-items, #mini-cart .cart-items-text{color:#0088cc;}.sticky-header #mini-cart .cart-subtotal, .sticky-header #mini-cart .minicart-icon{color:#0088cc;}.sticky-header #mini-cart .cart-items, .sticky-header #mini-cart .cart-items-text{color:#0088cc;}</style><style type="text/css" data-type="vc_custom-css">.footer-wrapper.fixed #footer .footer-bottom { background-color: rgba(255,255,255,0.8); padding-top: 22px; padding-bottom: 22px; background-color: #333333; color: #ffffff; }
#footer .footer-bottom a, #footer .footer-bottom .widget_nav_menu ul li:before { color: #f7f7f7; }
.people-join-positions > tbody > tr > td { padding: 8px 30px; cursor: pointer; }
.people-join-cv-input { border-radius: 0px; border-color: #e4e4e4; background-color: #f0f0f0; text-align: center; }
.people-join-cv-input.btn-file-input { /*padding: 2px 12px 6px 12px;*/ margin-bottom: 6px; }
.page-id-491 .wpcf7-form-control.wpcf7-submit.btn.btn-primary.btn-lg { border-radius: 0px; border-color: #e4e4e4; background-color: #f0f0f0; text-align: center; font-size: 14px; width: 100%; color: #000; }
.div_uploadsec p:empty { display: none !important; }
.div_uploadsec span.people-join-cv-input { font-size: 14px !important; }
#divbrowsebtn { display: inline-block; }
.div_uploadsec span.people-join-cv-input p { width: auto !important; display: inline-block; margin-bottom: 0; margin-left: 21px; }
.div_uploadsec span.people-join-cv-input p:hover { text-decoration: underline; }
.div_checkboxsec p:empty { display: none !important; }
#rgpd-text { margin-top: 10px; }
#rgpd-text p span { color: #979695; }
.div_checkboxsec .wpcf7-list-item { margin-right: 4px !important; }
.people-join-cv-checkbox label { font-size: 8.5pt; color: #979695; }
.page-id-491 a { color: unset !important; }
select.people-join-cv-input { color: #000; }
#myModal .modal-dialog.modal-sm { margin-top: 100pt; }
#myModal .modal-content { width: 394px; border: 1px solid #eee; padding: 10px; padding: 25px; padding-bottom: 15px !important; height: 208px;}
.modal-content .text-right a { margin-right: 13px !important; color:#fff !important;font-size: 14px !important;}
.modal-content textarea {border: 1px solid #eee !important;height: 116px;}
.divuploadcv {display:none !important;}

.people-join-cv-input .btn-file-input-link.btn-file-input-link-text {text-decoration:underline !important;}
.people-join-cv-input #divbrowsebtn {text-decoration: underline;margin-left: 10px;margin-right: 10px;}
.wpcf7-list-item input[type="checkbox"]{width:auto !important;top: 3px;position: relative;}</style><style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1558384747700{margin-top: -27px !important;padding-top: 150px !important;padding-bottom: 150px !important;}.vc_custom_1558553702625{padding-top: 20px !important;padding-bottom: 25px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript></head>
<body data-rsssl=1 class="page-template-default page page-id-781 page-child parent-pageid-756 wp-embed-responsive full blog-9  wpb-js-composer js-comp-ver-5.7 vc_responsive">

	<div class="page-wrapper"><!-- page wrapper -->

		
					<!-- header wrapper -->
			<div class="header-wrapper">
								<header id="header" class="header-2">
	
	<div class="header-main">
		<div class="container">
			<div class="header-left">
						<div class="logo">
		<a href="https://www.udon.com/pt/en/" title="pt - Portugal"  rel="home">
					<img class="img-responsive sticky-logo sticky-retina-logo" src="//www.udon.com/pt/wp-content/uploads/sites/9/2019/05/logo-udon-negro.jpg" alt="pt" /><img class="img-responsive standard-logo retina-logo" src="//www.udon.com/pt/wp-content/uploads/sites/9/2019/05/logo-udon-negro.jpg" alt="pt" />			</a>
			</div>
					</div>
			<div class="header-center">
				<div id="main-menu">
					<ul id="menu-inicio-eng" class="main-menu mega-menu show-arrow"><li id="nav-menu-item-852" class="menu-item menu-item-type-post_type menu-item-object-page narrow"><a href="https://www.udon.com/pt/en/menu/" class="">Menu</a></li>
<li id="nav-menu-item-851" class="menu-item menu-item-type-post_type menu-item-object-page narrow"><a href="https://www.udon.com/pt/en/ingredient/" class="">Ingredient</a></li>
<li id="nav-menu-item-850" class="menu-item menu-item-type-post_type menu-item-object-page narrow"><a href="https://www.udon.com/pt/en/restaurants/" class="">Restaurants</a></li>
</ul>					<a class="mobile-toggle"><i class="fas fa-bars"></i></a>
				</div>
			</div>
			<div class="header-right">
				<div>
									</div>

				
			</div>
		</div>
		
<div id="nav-panel" class="">
	<div class="container">
		<div class="mobile-nav-wrap">
			<div class="menu-wrap"><ul id="menu-inicio-eng-1" class="mobile-menu accordion-menu"><li id="accordion-menu-item-852" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.udon.com/pt/en/menu/" class="">Menu</a></li>
<li id="accordion-menu-item-851" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.udon.com/pt/en/ingredient/" class="">Ingredient</a></li>
<li id="accordion-menu-item-850" class="menu-item menu-item-type-post_type menu-item-object-page"><a href="https://www.udon.com/pt/en/restaurants/" class="">Restaurants</a></li>
</ul></div>		</div>
	</div>
</div>
	</div>
</header>

							</div>
			<!-- end header wrapper -->
		
		
		<div id="main" class="column1 boxed no-breadcrumbs"><!-- main -->

			<div class="container">
			<div class="row main-content-wrap">

			<!-- main content -->
			<div class="main-content col-lg-12">

			
	<div id="content" role="main">
				
			<article class="post-781 page type-page status-publish hentry">
				
				<span class="entry-title" style="display: none;">Join the team</span><span class="vcard" style="display: none;"><span class="fn"><a href="https://www.udon.com/pt/en/blog/author/pymesworld/" title="Posts by PymesWorld" rel="author">PymesWorld</a></span></span><span class="updated" style="display:none">2019-05-31T14:28:07+00:00</span>
				<div class="page-content">
					<div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row row vc_custom_1558384747700"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"></div></div></div><div class="vc_row-full-width vc_clearfix"></div><!-- Row Backgrounds --><div class="upb_bg_img" data-ultimate-bg="url(https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/personas.jpg)" data-image-id="id^295|url^https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/personas.jpg|caption^null|alt^null|title^personas|description^null" data-ultimate-bg-style="vcpb-default" data-bg-img-repeat="no-repeat" data-bg-img-size="cover" data-bg-img-position="" data-parallx_sense="30" data-bg-override="0" data-bg_img_attach="scroll" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation" data-bg-animation-type="h" data-animation-repeat="repeat" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false"  data-custom-vc-row=""  data-vc="5.7"  data-is_old_vc=""  data-theme-support=""   data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""    ></div><div class="vc_row wpb_row row vc_custom_1558553702625"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="porto-u-heading" data-hspacer="no_spacer" data-halign="left" style="text-align:left"><div class="porto-u-main-heading"><h2 style="font-weight:700;color:#000000;font-size:24px;line-height:35px;">FIND YOUR JOB</h2></div></div><div class="vc_empty_space"   style="height: 20px" ><span class="vc_empty_space_inner"></span></div>
<div role="form" class="wpcf7" id="wpcf7-f784-p781-o1" lang="es-ES" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/pt/en/people/join-the-team/#wpcf7-f784-p781-o1" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="784" />
<input type="hidden" name="_wpcf7_version" value="5.1.4" />
<input type="hidden" name="_wpcf7_locale" value="es_ES" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f784-p781-o1" />
<input type="hidden" name="_wpcf7_container_post" value="781" />
</div>
<div class="div_mian_hoinform people-join-cv-form">
<div class="row">
<div class="col-xs-12 col-sm-4">
<div class="form-group">
        <span class="wpcf7-form-control-wrap cityname"><select name="cityname" class="wpcf7-form-control wpcf7-select required form-control custom-select-like people-join-cv-input" aria-invalid="false"><option value="">---</option><option value="Lisboa">Lisboa</option></select></span>
      </div></div>
<div class="col-xs-12 col-sm-4">
<div class="form-group div_uploadsec"><span class="btn btn-block btn-file-input people-join-cv-input">Upload your CV  <span class="wpcf7-form-control-wrap cv"><input type="file" name="cv" size="40" class="wpcf7-form-control wpcf7-file cv_file" accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.ppt,.pptx,.odt,.avi,.ogg,.m4a,.mov,.mp3,.mp4,.mpg,.wav,.wmv" aria-invalid="false" /></span><a href="javascript:void(0);" class="btn-file-input-link btn-file1" id="divbrowsebtn" onclick="jQuery('.cv_file').trigger('click')">attach</a><a class="btn-file-input-link btn-file-input-link-text" data-toggle="modal" data-target="#myModal">paste text</a>
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog modal-sm">
<div class="modal-content">
<div class="form-group error-container"><span class="wpcf7-form-control-wrap cv_text"><textarea name="cv_text" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea btn-file-input-textarea form-control" aria-invalid="false"></textarea></span></div>
<div class="text-right">
            <a href="#" class="btn btn-danger btn-file-input-link-modal-dismiss" data-dismiss="modal">Cancelar</a><a href="#" class="btn btn-success btn-file-input-link-modal-dismiss btn-file-input-textarea-accept" data-dismiss="modal">Save</a></div>
</div>
</div>
</div>
<p></span></div></div>
<div class="col-xs-12 col-sm-4">
<div class="form-group div_checkboxsec">
       <input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit btn btn-primary btn-lg" /></p>
<div id="rgpd-text" style="" data-legal="http://www.udon.es/es/info/legal#terms" data-privacy="https://www.udon.com/pt/legal/">
<p style="margin-bottom: 8.0pt; text-align: justify; line-height: 12.95pt; text-autospace: none;"><span style="font-size: 8pt;">The data provided will be processed by UDON to enable the management of your application, upon receiving your consent, and your data may be communicated to others. For more information or to exercise your rights as granted by the law (regarding access, rectification, deletion, limitation and opposition) you can access the Privacy Policy.</a></span></p>
</p></div>
<div class="checkbox people-join-cv-checkbox"><label><span class="wpcf7-form-control-wrap acceptform"><span class="wpcf7-form-control wpcf7-acceptance"><span class="wpcf7-list-item"><input type="checkbox" name="acceptform" value="1" aria-invalid="false" class="form-control" /></span></span></span>I have read and accept the <a href="https://www.udon.com/pt/legal/" target="_blank">Privacy Policy</a> </label>
        </div></div></div></div>
</div>
<style>
.cv_file{ display:none;}
</style>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div><div class="porto-u-heading" data-hspacer="no_spacer" data-halign="left" style="text-align:left"><div class="porto-u-sub-heading" style="font-weight:600;font-size:22px;line-height:28px;">You can also find other jobs at <a href="https://twitter.com/udonjobs" target="_blank" rel="noopener noreferrer">@udonjobs</a></div></div></div></div></div><div class="vc_row wpb_row row"><div class="vc_column_container col-md-12"><div class="wpb_wrapper vc_column-inner"><div class="vc_empty_space"   style="height: 150px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div>
				</div>
			</article>

			<div class="">
			
						</div>

		
	</div>

		

</div><!-- end main content -->



	</div>
	</div>


		
			
			</div><!-- end main -->

			
			<div class="footer-wrapper fixed">

				
				
				<div id="footer" class="footer-1"
>
	
		<div class="footer-bottom">
    
		<div class="container">
        
						<div class="footer-left">
									<span class="logo">
						<a href="https://www.udon.com/pt/en/" title="pt - Portugal">
							<img class="img-responsive" src="//www.udon.com/pt/wp-content/themes/porto/images/logo/logo_footer.png" alt="pt" />						</a>
					</span>
									<ul id="menu-footer-en" class="footer-copyright"><li id="menu-item-867" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-867"><a href="https://www.udon.com/pt/en/sobre-udon/">About Us</a></li>
<li id="menu-item-866" class="menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor menu-item-866"><a href="https://www.udon.com/pt/en/people/">People</a></li>
<li id="menu-item-865" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-865"><a href="https://www.udon.com/pt/en/franchises/">Franchises</a></li>
<li id="menu-item-864" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-864"><a href="https://www.udon.com/pt/en/faqs/">FAQS</a></li>
<li id="menu-item-863" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-863"><a href="https://www.udon.com/pt/en/contact/">contact</a></li>
<li id="menu-item-862" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-862"><a href="https://www.udon.com/pt/en/legal/">Legal</a></li>
</ul>			</div>
            
			            
            
            
                         <div class="FooterRight">
							<div class="textwidget"><ul class="list-inline right-side-links">
<li><a class="social-link" href="http://www.udon.es/es/gotorss/facebook" target="_blank" rel="noopener noreferrer"><img class="social_blackicon" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/facebook-logo.png" /><img class="social_whiteicon" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/facebook_999.png" /></a></li>
<li><a class="social-link" href="http://www.udon.es/es/gotorss/twitter" target="_blank" rel="noopener noreferrer"><img class="social_blackicon twitterimg" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/twitter_24.png" /><br />
<img class="social_whiteicon twitterimg" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/twitter_99.png" /><br />
</a></li>
<li><a class="social-link" href="http://www.udon.es/es/gotorss/instagram" target="_blank" rel="noopener noreferrer"><img class="social_blackicon" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/instagram_24.png" /><img class="social_whiteicon" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/instagram_999.png" /></a></li>
<li class="visible-xs clearfix"></li>
<li class="languages-links">
<div class="dropup open"><a class="dropdown-toggle" href="https://www.udon.com/pt/" data-toggle="dropdown"> es <i class="fa fa-angle-up" aria-hidden="true"></i> </a></p>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="https://www.udon.com/pt/pt-pt/">pt</a></li>
<li><a href="https://www.udon.com/pt/en/">en</a></li>
</ul>
</div>
</li>
<li class="country-sites-links">
<div class="dropup"><a class="dropdown-toggle" href="#" data-toggle="dropdown"> <img class="country-sites-links-image" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/pt.png" alt="PT" /> <i class="fa fa-angle-up" aria-hidden="true"></i></a></p>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="https://www.udon.com/mx" target="_blank" rel="noopener noreferrer"><img class="country-sites-links-image" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/06/mx.png" alt="mx" /> </a></li>
<li><a href="http://www.udon.es" target="_blank" rel="noopener noreferrer"> <img class="country-sites-links-image" src="https://www.udon.com/pt/wp-content/uploads/sites/9/2019/05/es.png" alt="ES" /></a></li>
</ul>
</div>
</li>
</ul>
</div>
		 			</div>

			
					</div>
	</div>
	</div>
<link rel="stylesheet" href="https://www.udon.com/pt/wp-content/themes/porto-child/ie7.css" type="text/css" media="all" />
				
			</div>

		
	</div><!-- end wrapper -->
	

<!--[if lt IE 9]>
<script src="https://www.udon.com/pt/wp-content/themes/porto/js/libs/html5shiv.min.js"></script>
<script src="https://www.udon.com/pt/wp-content/themes/porto/js/libs/respond.min.js"></script>
<![endif]-->

<!--
The IP2Location Redirection is using IP2Location LITE geolocation database. Please visit https://lite.ip2location.com for more information.
-->

<!-- Facebook Pixel Event Code -->
<script>
  document.addEventListener(
    'wpcf7submit',
    function (event) {
  fbq('track', 'Lead', {
    "fb_integration_tracking": "contact-form-7"
});
},
    false
  );
</script>
<!-- End Facebook Pixel Event Code -->
      <script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/www.udon.com\/pt\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var porto_live_search = {"nonce":"9fd16e7feb"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/inc/lib/live-search/live-search.js?ver=4.9.7'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-includes/js/comment-reply.min.js?ver=5.2.6'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js?ver=5.7'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/popper.min.js?ver=1.12.5'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/bootstrap.js?ver=4.1.3'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/jquery.cookie.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/owl.carousel.min.js?ver=2.3.4'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/jquery.appear.min.js'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/jquery.fitvids.min.js?ver=1.1'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/jquery.matchHeight.min.js'></script>
<script type='text/javascript' async="async" src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/modernizr.js?ver=2.8.3'></script>
<script type='text/javascript' async="async" src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/jquery.magnific-popup.min.js?ver=1.1.0'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/libs/jquery.waitforimages.min.js?ver=2.0.2'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var js_porto_vars = {"rtl":"","ajax_url":"https:\/\/www.udon.com\/pt\/wp-admin\/admin-ajax.php","change_logo":"1","container_width":"1170","grid_gutter_width":"30","show_sticky_header":"","show_sticky_header_tablet":"1","show_sticky_header_mobile":"1","ajax_loader_url":"\/\/www.udon.com\/pt\/wp-content\/themes\/porto\/images\/ajax-loader@2x.gif","category_ajax":"","prdctfltr_ajax":"","show_minicart":"0","slider_loop":"1","slider_autoplay":"1","slider_autoheight":"1","slider_speed":"5000","slider_nav":"","slider_nav_hover":"1","slider_margin":"","slider_dots":"1","slider_animatein":"","slider_animateout":"","product_thumbs_count":"4","product_zoom":"1","product_zoom_mobile":"1","product_image_popup":"1","zoom_type":"inner","zoom_scroll":"1","zoom_lens_size":"200","zoom_lens_shape":"square","zoom_contain_lens":"1","zoom_lens_border":"1","zoom_border_color":"#888888","zoom_border":"0","screen_lg":"1200","mfp_counter":"%curr% of %total%","mfp_img_error":"<a href=\"%url%\">The image<\/a> could not be loaded.","mfp_ajax_error":"<a href=\"%url%\">The content<\/a> could not be loaded.","popup_close":"Close","popup_prev":"Previous","popup_next":"Next","request_error":"The requested content cannot be loaded.<br\/>Please try again later.","loader_text":"Loading...","submenu_back":"Back","porto_nonce":"80d8548018"};
/* ]]> */
</script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-content/themes/porto/js/theme.js?ver=4.9.7'></script>
<script type='text/javascript' async="async" src='https://www.udon.com/pt/wp-content/themes/porto/js/theme-async.js?ver=4.9.7'></script>
<script type='text/javascript' src='https://www.udon.com/pt/wp-includes/js/wp-embed.min.js?ver=5.2.6'></script>

	<script>
		jQuery(document).ready(function(){});	</script>
<script>
jQuery(document).ready(function(e) {
    jQuery(".home .standard-logo").attr("src",'https://www.udon.com/pt/wp-content/themes/porto-child/logo_white.png');
	jQuery(".home .rightmtop img,.home .mob-standard-logo").attr("src",'https://www.udon.com/pt/wp-content/themes/porto-child/logo_white.png');
	//jQuery(".page-id-491 .wpcf7-submit").removeAttr("disabled");
	//jQuery('#divbrowsebtn').click(function(e){
		
	
});

function francheseFormBg(){
	
	var let_bg = jQuery('.franchises-background');
					
					let_bg.css("right","0px");

					var pr = jQuery(window).width() - (let_bg.offset().left + let_bg.outerWidth());
					let_bg.css( 'right', '-' + pr + 'px' );
}
</script>
<script>
jQuery(document).ready(function(e) {
    
var htmlLang ="";
	  htmlLang = '<div class="dropup open"><a class="dropdown-toggle" href="https://www.udon.com/pt/en" data-toggle="dropdown">EN<i class="fa fa-angle-up" aria-hidden="true"></i></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="https://www.udon.com/pt/pt-pt">pt</a></li><li><a href="https://www.udon.com/pt">es</a></li></ul></div>';
	 if(htmlLang!=""){
	
	jQuery(".languages-links").html(htmlLang);
}

});
</script>
</body>
</html>