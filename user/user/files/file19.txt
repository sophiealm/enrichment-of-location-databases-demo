https://hotelflordesal.com/en/politica-de-privacidade/
<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="O Hotel Flôr de Sal, no norte de Portugal, destaca-se pela vista para o mar e pelo SPA de água salgada. O espaço ideal para momentos de puro relaxamento." name="description">
  <meta name="geo.placename" content="Praia Norte" />
  <meta name="geo.position" content="41.692363; -8.848252" />
  <meta name="geo.region" content="PT-Praia Norte" />
  <meta name="geo.country" content="PT" />
  <meta name="ICBM" content="41.692363, -8.848252" />
  <meta name="robots" content="index" />


<!-- if page is content page -->
  <meta property="og:title" content="Privacy Policy | Hotel Flôr de Sal: o seu hotel e SPA com o mar de companhia" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://hotelflordesal.com/" />
  <meta property="og:image" content="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/logo_facebook.png" />
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:image:width" content="250"> 
  <meta property="og:image:height" content="250">
  <meta property="og:description" content="O Hotel Flôr de Sal, no norte de Portugal, destaca-se pela vista para o mar e pelo SPA de água salgada. O espaço ideal para momentos de puro relaxamento." />
  <meta content="blisq.pt" name="author">
 
  <title>Privacy Policy | Hotel Flôr de Sal: o seu hotel e SPA com o mar de companhia</title>
  <link rel="pingback" href="https://hotelflordesal.com/xmlrpc.php">
<link rel='dns-prefetch' href='//s.w.org' />
<link rel='stylesheet' id='wp-block-library-css'  href='https://hotelflordesal.com/wp-includes/css/dist/block-library/style.min.css?ver=5.5.1' type='text/css' media='all' />
<link rel="https://api.w.org/" href="https://hotelflordesal.com/en/wp-json/" /><link rel="alternate" type="application/json" href="https://hotelflordesal.com/en/wp-json/wp/v2/pages/117" /><link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://hotelflordesal.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://hotelflordesal.com/wp-includes/wlwmanifest.xml" /> 
<link rel="canonical" href="https://hotelflordesal.com/en/politica-de-privacidade/" />
<link rel='shortlink' href='https://hotelflordesal.com/en/?p=117' />
<link rel="alternate" type="application/json+oembed" href="https://hotelflordesal.com/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhotelflordesal.com%2Fen%2Fpolitica-de-privacidade%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://hotelflordesal.com/en/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fhotelflordesal.com%2Fen%2Fpolitica-de-privacidade%2F&#038;format=xml" />
<style type="text/css">
.qtranxs_flag_pt {background-image: url(https://hotelflordesal.com/wp-content/plugins/qtranslate-x/flags/pt.png); background-repeat: no-repeat;}
.qtranxs_flag_en {background-image: url(https://hotelflordesal.com/wp-content/plugins/qtranslate-x/flags/gb.png); background-repeat: no-repeat;}
</style>
<link hreflang="pt" href="https://hotelflordesal.com/pt/politica-de-privacidade/" rel="alternate" />
<link hreflang="en" href="https://hotelflordesal.com/en/politica-de-privacidade/" rel="alternate" />
<link hreflang="x-default" href="https://hotelflordesal.com/politica-de-privacidade/" rel="alternate" />
<meta name="generator" content="qTranslate-X 3.4.6.8" />
  <link href='https://fonts.googleapis.com/css?family=Neuton' rel='stylesheet' type='text/css'>
  <link rel="icon" type="image/png" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/favicon.png" />
  <link rel="stylesheet" type="text/css" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/css/bootstrap.css"  media="screen"> 
  <link rel="stylesheet" type="text/css" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/slider/css/flexslider.css"  media="screen" />
  <link rel="stylesheet" type="text/css" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/css/jquery.bxslider.css">
  <link rel="stylesheet" type="text/css" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/css/blisq.css?v=2020">
  <link rel="stylesheet" type="text/css" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/css/animate.css">
  <link rel="stylesheet" type="text/css" href="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/vegas/vegas.min.css">
  <script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/jquery-2.1.3.min.js"></script>
  <script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/vegas/vegas.js"></script>
  <script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/jquery.bxslider.min.js"></script>
  <script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/js.cookie-min.js"></script>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/shiv/html5shiv.js"></script>
    <script src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/shiv/respond.min.js"></script>
  <![endif]-->

<script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#8b8a88",
      "text": "#ffffff"
    },
    "button": {
      "background": "#730c1f",
      "text": "#ffffff"
    }
  },
  "position": "bottom-left",
  "content": {
    "message": "Este site utiliza cookies para lhe proporcionar uma melhor experiência. Ao navegar no mesmo, está a consentir a sua utilização.",
    "dismiss": "Entendi",
    "link": "Ler mais",
    "href": "https://hotelflordesal.com/politica-de-privacidade/"
  }
})});
</script> 
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68683997-26', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body class="privacy-policy page-template page-template-template-politica page-template-template-politica-php page page-id-117">
<!--POPUP-->
	<style>
	.popup-btn-close{
	    background: transparent;
	    border: none;
	    color: #fff;
	    font-weight: 700;
	    font-size: 13px;
	    padding: 8px 15px;
	    cursor: pointer;
	    letter-spacing: .9px;
	    display: block;
	}
	.popup-btn-close:hover{
		background: transparent;
		color:#fff;
	}	
.popup-abertura,
.popup-pub-abertura,
.popup-video-abertura {
    background: rgba(255, 255, 255, .9)
}
.popup-btn-close,
.popup-pub-btn-close,
.popup-video-btn-close {
    margin-bottom: 20px;
    top: -35px;
    font-size: 13px;
    cursor: pointer;
    right: 0
}
.popup-abertura {
    display: none;
    position: fixed;
    z-index: 9999999999999999;
    width: 100%;
    height: 100%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 10%
}

.popup-container {
    position: relative
}

.popup-btn-close {
    display: block;
    color: #9a9a9a;
    position: absolute;
    z-index: 9999
}

.popup-btn-close:hover {
    color: #069adf
}

.popup-pub-abertura {
    display: none;
    position: fixed;
    z-index: 9999999999999999;
    height: auto;
    padding: 0;
    bottom: 120px;
    left: 30px;
    width: 320px
}

.popup-pub-container {
    position: relative;
    width:488px;
}

.popup-pub-btn-close {
    display: block;
    color: #9a9a9a;
    position: absolute;
    z-index: 9999
}

.popup-pub-btn-close:hover {
    color: #069adf
}

.popup-video-abertura {
    display: none;
    position: fixed;
    z-index: 9999999999999999;
    width: 100%;
    height: 100%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    padding: 10%
}



.popup-video-container {
    position: relative
}

.popup-video-iframe-container {
    overflow: hidden;
    width: 720px;
    height: 405px
}

.popup-video-container iframe {
    width: 100%;
    height: 100%;
    padding-bottom: 38px
}

.popup-video-btn-close {
    display: block;
    color: #9a9a9a;
    position: absolute;
    z-index: 9999
}	
.popup-abertura img{
	width: 100%;
}
.popup-img-abertura{
	max-width: 450px;
}
</style>	
	<div class="popup-abertura">
        <div class="popup-container">
        	<span class="popup-btn-close"><span id="seconds">20</span> - FECHAR</span>
       		<a href="https://hotelflordesal.com/finalmente-voltamos-a-reencontrar-nos/" target="_blank">
       			<img class="popup-img-abertura" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/reabertura-hotel-2020.jpg" />
       		</a>
       	</div>
     </div>
	<script>
		jQuery(document).ready(function($) {
			$(".popup-btn-close").click(function() { 
				$(".popup-abertura").remove();
			});
			
			if(!Cookies.get('popup')){
				$(".popup-abertura").css("display", "flex").hide().fadeIn();
				Cookies.set('popup', 'on', {path: '/'});
			}else{
				$(".popup-abertura").remove();
			}
			function countDown(){
		        seconds--;
		        $("#seconds").text(seconds);
		        if (seconds === 0){
					$(".popup-abertura").remove();
		          	clearInterval(i);
		          	return;
		        }
		    }
		    var seconds = 20,
	        i = setInterval(countDown, 1000);			
		}); 
	</script>

<!--END POPUP-->
<div class="loader">
	<img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/logo_loader.png">
</div>
<div class="reservas-mobile">
	<div class="reservas">
        <center><a href="https://secure.guestcentric.net/api/bg/book.php?apikey=9387b609de5bb54b9cf6c7447085178d&s=default&l=pt_PT" target="_blank">Book Online</a></center>
     </div>
</div>
<nav id="lateral-nav">
	<ul class="navigation" id="menu-main-menu"><li><a href="https://hotelflordesal.com/en/hotel/">Hotel</a></li><li><a href="https://hotelflordesal.com/en/quartos/">Bedrooms</a></li><li><a href="https://hotelflordesal.com/en/saleiro/">Saleiro</a></li><li><a href="https://hotelflordesal.com/en/corporate/">Corporate</a></li><li><a href="https://hotelflordesal.com/en/health-club-spa/">Health Club & SPA</a></li><li><a href="https://hotelflordesal.com/en/programas/">Programmes</a></li><li><a href="http://#">Gallery</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/areas-comuns/">Common Zones</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/quartos/">Bedrooms</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/restaurante/">Restaurant</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/health-club-spa/">Fitness Center</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/corporate/">Corporate</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/viana-do-castelo/">Viana do Castelo</a></li><li class="sub-li-mob"><a href="https://hotelflordesal.com/en/galeria/videos/">Videos</a></li><li><a href="https://hotelflordesal.com/en/hotel-360o/">Hotel 360º</a></li><li class="vertical-align-bottom">
				<a href="https://www.facebook.com/hotelflordesal" target="_blank"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/face.png"></a>
			</li>
			<li class="vertical-align-bottom">
				<a href="https://www.instagram.com/hotelflordesal/" target="_blank"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/insta.png"></a>
			</li>
			<br></ul>  
</nav>
<div class="header">
  <a href="#0" id="menu-trigger">
    <span class="menu-icon"></span>
  </a>
  <div class="container">
    <div class="logo">
      <a href="https://hotelflordesal.com/en"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/logo.png"></a>
    </div>
    <nav class="menu menu--maria">
    	<ul id="menu-menu_top" class="menu__list"><li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28 menu__item"><a href="https://hotelflordesal.com/en/hotel/">Hotel</a></li>
<li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29 menu__item"><a href="https://hotelflordesal.com/en/quartos/">Bedrooms</a></li>
<li id="menu-item-30" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30 menu__item"><a href="https://hotelflordesal.com/en/saleiro/">Saleiro</a></li>
<li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31 menu__item"><a href="https://hotelflordesal.com/en/corporate/">Corporate</a></li>
<li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32 menu__item"><a href="https://hotelflordesal.com/en/health-club-spa/">Health Club &#038; SPA</a></li>
<li id="menu-item-540" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-540 menu__item"><a href="https://hotelflordesal.com/en/programas/">Programmes</a></li>
<li id="menu-item-137" class="menu-item menu-item-type-custom menu-item-object-custom dropdown menu-item-137 menu__item"><a href="http://#">Gallery</a>
<ul class="sub-menu">
	<li id="menu-item-55" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-55 menu__item"><a href="https://hotelflordesal.com/en/galeria/areas-comuns/">Common Zones</a></li>
	<li id="menu-item-54" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-54 menu__item"><a href="https://hotelflordesal.com/en/galeria/quartos/">Bedrooms</a></li>
	<li id="menu-item-53" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-53 menu__item"><a href="https://hotelflordesal.com/en/galeria/restaurante/">Restaurant</a></li>
	<li id="menu-item-52" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-52 menu__item"><a href="https://hotelflordesal.com/en/galeria/health-club-spa/">Fitness Center</a></li>
	<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51 menu__item"><a href="https://hotelflordesal.com/en/galeria/corporate/">Corporate</a></li>
	<li id="menu-item-50" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-50 menu__item"><a href="https://hotelflordesal.com/en/galeria/viana-do-castelo/">Viana do Castelo</a></li>
	<li id="menu-item-49" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-49 menu__item"><a href="https://hotelflordesal.com/en/galeria/videos/">Videos</a></li>
</ul>
</li>
<li id="menu-item-694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-694 menu__item"><a href="https://hotelflordesal.com/en/hotel-360o/">Hotel 360º</a></li>
</ul>
    </nav>
    <div class="header-right">
      <div class="lang">
        <div class="pt">
          <a href="https://hotelflordesal.com/pt">PT</a>
        </div>
        <div class="en">
          <a href="https://hotelflordesal.com/en">EN</a>
        </div>
      </div>
      <div class="reservas">
        <center><a href="https://secure.guestcentric.net/api/bg/book.php?apikey=9387b609de5bb54b9cf6c7447085178d&s=default&l=pt_PT" target="_blank">Book Online</a></center>
      </div>
    </div>
  </div>
</div>
<div class="main-content cinza">


 <!-- Conteudo-->
<div class="main-content branco">
  <div class="topo">
    <img src="https://hotelflordesal.com/wp-content/uploads/2016/09/slide.jpg">
    <div class="programas-header">
        
          
          <div id="hide-programas"class="button btn_todos_eventos"><a href="https://hotelflordesal.com/flordesal/programas/">See everything</a></div>        
          <button id="show-programas"  class="button btn_todos_eventos">Programmes</button>
          <div class="slider_eventos slide_eventos_descricao prog-popup">
            <section class="slider">
              <div class="flexslider slide-eventos">          
                <ul class="slides">
                	<span id="fechar-programas-popup"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/botao_close_programas.png" /></span>

				                  <li>
			          <div class="descricao">
			            <h4 class="data"><a href="https://hotelflordesal.com/en/programas/viana-fica-no-coracao/ ">Viana fica no coração</a></h4>
			            <p>Quem gosta vem, quem ama fica.</p>
			            <p><a class="book-now" href="https://secure.guestcentric.net/api/bg/book.php?apikey=9387b609de5bb54b9cf6c7447085178d&s=default&l=pt_PT" target="_blank">Book Now!</a></p>
			          </div>
		          </li>			          

					                </ul>
              </div>
            </section>
          </div> 
          
    </div>
  </div>
  <div class="frase branco">
    		<div class="red">Privacy Policy</div>
    		<div class="italico"></div>
  </div>
  <div class="content_right" id="restaurante_saleiro">
    <div class="container">
      <div class="col-sm-6 col-xs-12">
        <div class="top_left noticia">
			<p><strong>Notice about website content </strong><br />
Before using the Hotel Flôr de Sal website, the user should carefully read the Terms and Conditions. Upon visiting and using the website, the user declares that s/he has read, understood and accepted the Terms and Conditions described below, without the need for any subsequent action or consent.<br />
The information contained in this website is the property of the Hotel Flôr de Sal, which manages the respective content with total discretion and within the framework of Portuguese law, reserving the right to modify, suspend or cancel such content, with no advance notice, as well as these applicable terms and conditions. The partial or complete download, reproduction or forwarding of the content of this website for commercial gain is strictly forbidden.<br />
Users of this website cannot use any software or application that interferes with the normal functioning of the site. No application, automatic mechanism or manual process may be implemented to monitor or copy its pages without prior consent and permission from the Hotel Flôr de Sal.<br />
Users may input usernames and passwords to access certain restricted access content. The restricted access content areas are confidential to the Hotel Flôr de Sal and the user may not use them for the purposes of making profit or doing business.</p>
<p>&nbsp;</p>
<p><strong>Access and availability of the services. Links to external websites</strong><br />
This website may contain links to other internet locations or websites. The Hotel Flôr de Sal does not accept responsibility for the accessibility and/or availability of these external resources to their website or content. Should problems occur, we recommend that the user contacts the respective administrator or webmaster.<br />
The Hotel Flôr de Sal reserves the right to modify the information, services, products and other content on this website, or to close this site at any time without the need for advance notice.<br />
The Hotel Flôr de Sal shall not accept responsibility for any losses or damages resulting from the use and functioning of the website (including interruption in access to the site through action or facts imputable to third parties, for technical reasons or inefficiency, or for any other reasons not attributable to the Hotel Flôr de Sal). This waiving of responsibility includes, without limitation, damages caused by lost business, lost profits, interruption in business, loss of commercial information or other cash losses.</p>
<p>&nbsp;</p>
<p><strong>Privacy Policy and Personal Information</strong><br />
The Hotel Flôr de Sal undertakes to provide a high-quality service to its website visitors, and to ensure their privacy.<br />
Personal data will be requested to obtain access/make bookings at the Hotel Flôr de Sal. All personal information shall be used only to make the respective booking. The personal contacts provided are used by our database to send information solely on an internal basis about promotions, and are not transmitted to third parties. If you are a client registered on our website, you can change your personal data whenever you want.<br />
Any personal data collected by the Hotel Flôr de Sal through the website will be processed under the terms of the laws in force and only upon agreement of the owner of the data.</p>
<p>&nbsp;</p>
<p><strong>Information protection</strong><br />
Cookies<br />
Whenever you visit our website, a text file (Cookie) is generated and saved on the disk of your computer. This file will make it easier and faster for you to access the site in the future. We also identify the technical information of your computer when you visit our website, such as your IP (Internet Protocol), the operating system and the browser type. This information is used to improve the quality of your visit to our website, and will not be disclosed to external entities. Most browsers automatically accept these files (Cookies), but may delete them or automatically define that they are blocked. In the Help menu of your browser you can alter these settings. However, if you do not allow the use of cookies you may not be able to use some functionalities.</p>
<p>Security<br />
To guarantee the security of your data and maximum confidentiality, we process the information that you supply to us with the utmost confidentiality, in accordance with our internal security and confidentiality policies and procedures.</p>
<p>Applicable law<br />
These General Terms and Conditions of Usage are governed by Portuguese law.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
        </div>
      </div>

    </div>
  </div>
 
   		<div class="newsletter">
		    <div class="container">
		        <div class="col-sm-3">
		          <h4>Subscribe our newsletter</h4>
		        </div>
		        <form action="//hotelflordesal.us13.list-manage.com/subscribe/post?u=dea364c1a0e189ced323f5b5f&amp;id=a44ba8db79" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			        <div class="col-sm-3">
			          <input name='FNAME' placeholder='Name' type='text'>
			        </div>
			        <div class="col-sm-4">
			          <input name='EMAIL' placeholder='Email' type='text'>
			        </div>
			        <div class="col-sm-2">
			          <input name="subscribe" type='submit' value='Submit'>
			        </div>
			        <div class="col-sm-12"><input type="checkbox" id="gdpr_11685" name="gdpr[11685]" value="Y" }="" class="av-checkbox" required=""></div>
		        </form>
		    </div>
		 </div>
		 
	 
		<div class="footer">
	<div class="container">
		<div class="footer_left">
			<p class="red">
				Bookings			</p>
			<p class="telefone"><b>T</b> 258 800 100</p>
			<p class="telefone"><b>F</b> 258 800 101</p>
			<p class="telefone"><a href="mailto:reservas@hotelflordesal.com" >reservas@hotelflordesal.com</a></p>
		</div>

		<div class="footer_right">
			<ul class="navigation" id="menu-menu-bottom"><li><a href="https://www.tur4all.pt/resources/hotel-flor-de-sal">Acessibilidade</a></li><li><a href="https://hotelflordesal.com/en/politica-de-privacidade/">Privacy Policy</a></li><li><a href="https://hotelflordesal.com/en/recrutamento/">Join Us</a></li><li><a href="https://hotelflordesal.com/en/parceiros/">Partners</a></li><li><a href="https://hotelflordesal.com/en/media-kit/">Media Kit</a></li><li><a href="https://hotelflordesal.com/en/noticias/">News</a></li><li><a href="https://hotelflordesal.com/en/contactos/">Contacts</a></li><li class="vertical-align-bottom">
				<a href="https://www.facebook.com/hotelflordesal" target="_blank"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/face.png"></a>
			</li>
			<li class="vertical-align-bottom">
				<a href="https://www.instagram.com/hotelflordesal/" target="_blank"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/insta.png"></a>
			</li>
			<br></ul>
		</div>
	</div>
	<div class="container">
		<p>O Hotel Flôr de Sal encontra-se registado<br/> no registo nacional de empreendimentos turísticos com o nº23.</p>
	</div>
	<div class="container sub-footer">
		<img class="float-left" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/logos_rodape.png">
		<a target="_blank" href="https://www.tripadvisor.com.br/Hotel_Review-g189185-d633131-Reviews-Flor_de_Sal_Hotel-Viana_do_Castelo_Viana_do_Castelo_District_Northern_Portugal.html">
			<img width="135px" class="float-right" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/Hall of Fame TripAdvisor 2014 2015 2016 2017 2018.png">
			<!--<img  class="float-right" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/Selo_PME_Lider_2008-2016.jpg">-->
		</a>
	</div>
</div>
  
  


<div class="footer-blisq">
	 <a href="http://www.blisq.pt" target="_blank"><img src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/images/blisq_logo.png"></a>       
</div>
<script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/bootstrap/bootstrap.min.js"></script>
<script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/cookies/cookiechoices.js"></script>
<script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/slider/js/jquery.flexslider.js"></script>
<script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/flexslider.js"></script>
<script  type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/blisq/blisq.js"></script>
<script type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/menu.js"></script>
<script type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/effect.js"></script>
<script type="text/javascript" src="https://hotelflordesal.com/wp-content/themes/HFSAL_By_Blisq_Creative/js/cookies.js"></script>
<script type='text/javascript' src='https://hotelflordesal.com/wp-includes/js/wp-embed.min.js?ver=5.5.1' id='wp-embed-js'></script>
</body>
</html>

